﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApplication.Core;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class ApiController : Controller
    {
        [HttpPost]
        public async Task<JsonResult> Click(int id)
        {
            await ContentFromDB.AddView(id);
            return Json(true);
        }

        [HttpPost]
        [Authorize]
        public async Task<JsonResult> Favorits(string tagid, bool add)
        {
            if (String.IsNullOrWhiteSpace(tagid) || !Utils.IsCorrectTagName(tagid))
            {
                return Json(false);
            }

            string userId = User.Identity.GetUserId();

            ApplicationDbContext db = new ApplicationDbContext();
            ApplicationUser user = await db.Users.FirstOrDefaultAsync(a => a.Id == userId);
            Tag tag = await db.Tags.FindAsync(tagid);

            if (tag != null && user != null)
            {
                if (add)
                {
                    try
                    {
                        user.Tags.Add(tag);
                        await db.SaveChangesAsync();
                    }
                    catch(Exception ex)
                    {
                        Console.WriteLine(ex);
                        return Json(false);
                    }
                }
                else
                {
                    try
                    {
                        user.Tags.Remove(tag);
                        await db.SaveChangesAsync();
                    }
                    catch
                    {
                        return Json(false);
                    }
                }

            }
            return Json(true);
        }

        [HttpPost]
        [Authorize]
        public async Task<JsonResult> Bookmark(int id,string tag, bool add)
        {
            if (String.IsNullOrWhiteSpace(tag) || !Utils.IsCorrectTagName(tag)) return null;

            int changes = 0;
            string userId = User.Identity.GetUserId();

            if (add)
            {
                changes = await ContentFromDB.AddBookmark(userId, tag, id);
                if (changes > 0)
                {
                    changes = await ContentFromDB.UpdateRating(id, 0, 0, 1);
                }
            }
            else
            {
                changes = await ContentFromDB.RemoveBookmark(userId, id);
                if (changes > 0)
                {
                    changes = await ContentFromDB.UpdateRating(id, 0, 0, -1);
                }
            }

            return Json(changes > 0 ? true : false);
        }

        [HttpPost]
        [Authorize]
        public async Task<JsonResult> Like(int id, bool add)
        {
            int changes = 0;
            string userId = User.Identity.GetUserId();

            if (add)
            {
                changes = await ContentFromDB.AddLike(userId, id);
                if (changes > 0)
                {
                    changes = await ContentFromDB.UpdateRating(id, 0, 1, 0);
                }
            }
            else
            {
                changes = await ContentFromDB.RemoveLike(userId, id);
                if (changes > 0)
                {
                    changes = await ContentFromDB.UpdateRating(id, 0, -1, 0);
                }
            }

            return Json(changes > 0 ? true : false);
        }

        // Список тэегов для меню.
        [HttpPost]
        [Authorize]
        public async Task<JsonResult> UserData()
        {
            string userId = User.Identity.GetUserId();
            ContentFromDB.UserData items = await ContentFromDB.UsersData(userId);
            JsonResult resilt = Json(items);
            return resilt;
        }
    }
}