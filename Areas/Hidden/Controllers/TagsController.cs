﻿using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication.Areas.Hidden.Models;
using WebApplication.Models;
using Newtonsoft.Json;
using System.IO;

namespace WebApplication.Areas.Hidden.Controllers
{
    [Authorize(Roles = "Admin")]
    public class TagsController : Controller
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        // GET: Hidden/Tags
        ApplicationDbContext db = null;

        public TagsController()
        {
            db = new ApplicationDbContext();
        }

        public ActionResult Index()
        {
            DateTime last = DateTime.Now.AddDays(-2);
            List<Tag> tags = db.Tags.Where(a => a.ParentTags.Count == 0 && a.CreationDate < last && a.Feeds.Any(b=>b.DoNotUpdate == false) && a.Visible == true).ToList();
            ViewBag.Tags = tags;

            return View(new TagsSortParams() {
                Title = String.Empty,
                CreationDate = last,
                HasChildren = true,
                HasParent = true,
                Visible = true
            });
        }

        [HttpPost]
        public ActionResult Index([Bind(Include="Title,Visible,HasParent,HasChildren,CreationDate")]TagsSortParams sortparams)
        {
            
            var query = db.Tags.Where(a => a.Visible == sortparams.Visible);

            if (!String.IsNullOrWhiteSpace(sortparams.Title))
                query = query.Where(a => a.Title.Contains(sortparams.Title));

            if (!sortparams.HasParent)
                query = query.Where(a => a.ParentTags.Count == 0);

            if (!sortparams.HasChildren)
                query = query.Where(a => a.ChildTags.Count == 0);

            if (sortparams.CreationDate != null)
                query = query.Where(a => a.CreationDate >= sortparams.CreationDate);

            //ViewBag.Tags = query.Where(a=>a.Feeds.Any(b=>b.DoNotUpdate == false)).ToList();
            ViewBag.Tags = query.ToList();
            return View(sortparams);
        }

        // GET: Hidden/Tags/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Hidden/Tags/Create
        [HttpPost]
        public ActionResult Create([Bind(Include ="Id,Title,Visible,Description")]Tag tag)
        {
            if (ModelState.IsValid)
            {
                tag.CreationDate = DateTime.Now;
                db.Tags.Add(tag);
                db.SaveChanges();
            }

            return View(tag);
        }

        // GET: Hidden/Tags/Edit/5
        public ActionResult Edit(string id)
        {
            ViewBag.Tags = db.Tags.Where(a => a.Visible == true && a.Feeds.Any(b => b.DoNotUpdate == false && a.ParentTags.Count == 0)).ToList();

            Tag tag = db.Tags.Include("ParentTags").FirstOrDefault(a => a.Id == id);
            return View(tag);
        }

        // POST: Hidden/Tags/Edit/5
        [HttpPost]
        public ActionResult Edit([Bind(Include = "Id,Title,Visible,Description,CreationDate")] Tag tag,string[] ParentTags)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tag).State = EntityState.Modified;
                var existingParents = db.Tags.Include("ParentTags").FirstOrDefault(a => a.Id == tag.Id).ParentTags.ToList();
                existingParents.ForEach(a => tag.ParentTags.Remove(a));

                if (ParentTags != null && ParentTags.Count() > 0)
                {
                    var parentTags = db.Tags.Where(a => ParentTags.Contains(a.Id)).ToList<Tag>();
                    parentTags.ForEach(a => tag.ParentTags.Add(a));
                }
                
                db.SaveChanges();
            }

            ViewBag.Tags = db.Tags.Where(a => a.Visible == true && a.Feeds.Any(b => b.DoNotUpdate == false && a.ParentTags.Count == 0)).ToList();
            return View(tag);
        }

        public bool ChangeTagStatus(string tag,bool visible)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            Tag t = db.Tags.FirstOrDefault(a => a.Id == tag);
            if (t != null)
            {
                t.Visible = visible;
                db.SaveChanges();
            }
            return true;
        }


        [HttpPost]
        public bool Delete(string tag)
        {
            try {
                db.Tags.Remove(db.Tags.Find(tag));
                db.SaveChanges();
                return true;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        
        class CategoryList {
            public int id;
            public string title;
            public IEnumerable<string> tags;
            public int? parent;
        }


        public string GenerateMenuJson()
        {
            
            ApplicationDbContext db = new ApplicationDbContext();

            var categories = db.Categories.Include("Tags").Select(a => new CategoryList
            {
                id = a.Id,
                title = a.Name,
                tags = a.Tags.Select(b=>b.Id),
                parent = a.Parent.Id
            }).ToList();

            var tags = db.Tags.Where(a=>a.Visible == true && a.ParentTags.Count == 0).Select(a => new {
                id= a.Id,
                title = a.Title
            }).ToList();

            var lang = db.Languages.Select(a => new {
                id = a.Id,
                englishName = a.EnglishName,
                localName = a.LocalName,
                russianName = a.RussianName
            }).ToList();

            string json = JsonConvert.SerializeObject(new {
                categories = categories,
                tags = tags,
                langs = lang
            });

            using (StreamWriter sw = new StreamWriter(new FileStream( Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Scripts","tags.json")  , FileMode.Create, FileAccess.Write)))
            {
                sw.WriteLine(json);
            }

            return json;
            
        }
    }
}
