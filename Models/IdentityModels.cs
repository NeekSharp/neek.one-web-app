﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApplication.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public virtual List<Content> Contents { get; set; }
        public virtual List<Content> Likes { get; set; }


        public virtual List<Tag> Tags { get; set; }

    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("PgContext", throwIfV1Schema: false)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("dbo");

            modelBuilder.Entity<IdentityUserLogin>()
                .HasKey(a => new { a.UserId, a.LoginProvider, a.ProviderKey })
                .Map(a => a.ToTable("AspNetUserLogins"));  

            modelBuilder.Entity<IdentityUserRole>()
                .HasKey(a => new { a.UserId, a.RoleId })
                .Map(a => a.ToTable("AspNetUserRoles"));

            modelBuilder.Entity<IdentityUserClaim>()
                .HasKey(a => a.Id)
                .Map(a => a.ToTable("AspNetUserClaims"));

            var roles = modelBuilder.Entity<IdentityRole>()
                .HasKey(a => a.Id)
                .Map(a => a.ToTable("AspNetRoles"));

            roles.HasMany(a => a.Users).WithRequired().HasForeignKey(b => b.UserId);

            var user = modelBuilder.Entity<IdentityUser>()
                .HasKey(a => a.Id)
                .Map(a => a.ToTable("AspNetUsers"));

            user.HasMany(u => u.Roles).WithRequired().HasForeignKey(ur => ur.UserId);
            user.HasMany(u => u.Claims).WithRequired().HasForeignKey(uc => uc.UserId);
            user.HasMany(u => u.Logins).WithRequired().HasForeignKey(uc => uc.UserId);
            user.Property(u => u.UserName).IsRequired();

            /*
                Many to Many relations
            */

            // TagContents
            modelBuilder.Entity<Content>()
                .HasMany<Tag>(a => a.Tags)
                .WithMany(b => b.Contents)
                .Map(ab =>
                {
                    ab.MapLeftKey("tag_id");
                    ab.MapRightKey("content_id");
                    ab.ToTable("tag_contents");
                });

            // ApplicationUserContents
            modelBuilder.Entity<ApplicationUser>()
                .HasMany<Content>(a => a.Contents)
                .WithMany(b => b.Users)
                .Map(ab =>
                {
                    ab.MapLeftKey("ApplicationUser_Id");
                    ab.MapRightKey("Content_Id");
                    ab.ToTable("ApplicationUserContents");
                });

            // CategoryTag
            modelBuilder.Entity<Tag>()
                .HasMany<Categories>(a => a.Categories)
                .WithMany(b => b.Tags)
                .Map(ab =>
                {
                    ab.MapLeftKey("tag_id");
                    ab.MapRightKey("category_id");
                    ab.ToTable("category_tag");
                });

            // TagsRelations
            /*
                parent_tag_id character varying(60) NOT NULL,
                child_tag_id character varying(60) NOT NULL,
            */
            modelBuilder.Entity<Tag>()
                .HasMany<Tag>(a => a.ParentTags)
                .WithMany(a => a.ChildTags)
                .Map(tt =>
                {
                    tt.MapLeftKey("child_tag_id");
                    tt.MapRightKey("parent_tag_id");
                    tt.ToTable("tags_relations");
                });

            /*
                user_id character varying(128) NOT NULL,
                content_id integer NOT NULL,
            */
            modelBuilder.Entity<ApplicationUser>()
                .HasMany<Content>(a => a.Likes)
                .WithMany(a => a.Likers)
                .Map(tt =>
                {
                    tt.MapLeftKey("user_id");
                    tt.MapRightKey("content_id");
                    tt.ToTable("likes");
                });

            modelBuilder.Entity<ApplicationUser>()
                .HasMany<Tag>(a => a.Tags)
                .WithMany(a => a.Users)
                .Map(tt =>
                {
                    tt.MapLeftKey("user_id");
                    tt.MapRightKey("tag_id");
                    tt.ToTable("user_tags");
                });

            // Categories - parent category
            modelBuilder.Entity<Categories>()
                .HasOptional(a => a.Parent)
                .WithMany()
                .HasForeignKey(a => a.ParentId);
        }



        public DbSet<Content> Contents { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Domain> Domains { get; set; }
        public DbSet<Feed> Feeds { get; set; }
        public DbSet<Categories> Categories { get; set; }
        public DbSet<Languages> Languages { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        
    }
}