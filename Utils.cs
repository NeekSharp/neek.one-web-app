﻿using NLog;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using WebApplication.Core;

namespace WebApplication
{
    public static class Utils
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static List<string> FilterTags(IEnumerable<string> array)
        {
            if (array == null || array.Count() == 0) return null;
            List<string> result = new List<string>();

            foreach (string value in array)
            {
                if (!value.All(a => Char.IsLetterOrDigit(a) || a == '-')) continue;
                else result.Add(value);
            }

            return result;
        }

        public static bool IsCorrectTagName(string tag)
        {
            HashSet<Char> chars = new HashSet<char>() { '-' };
            if (tag.All(a => Char.IsLetterOrDigit(a) || a == '-'))
                return true;
            else
                return false;
        }

        public static string CreateImageLink(string source)
        {
            if (source == null || source.Length < 5) return null;
            StringBuilder sb = new StringBuilder(60);
            sb.Append("/Content/Images/");
            sb.Append(source[0]);
            sb.Append(source[1]);
            sb.Append("/");
            sb.Append(source);
            sb.Append(".jpg");
            return sb.ToString();
        }

        public static Dictionary<int, ContentItem> GetContentFromPgSqlDatabase(string strcommand)
        {
            Dictionary<int, ContentItem> items = new Dictionary<int, ContentItem>();

            using (NpgsqlConnection cn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["PgContext"].ConnectionString))
            {
                cn.Open();

                NpgsqlCommand command = new NpgsqlCommand(strcommand, cn);
                using (NpgsqlDataReader dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        int id = 0;
                        if (Int32.TryParse(dataReader["Id"].ToString(), out id))
                        {
                            if (!items.ContainsKey(id))
                            {
                                string image = null;
                                try
                                {
                                    if (dataReader["ImageHashName"] != null && dataReader["ImageHashName"] != DBNull.Value)
                                    {
                                        image = Utils.CreateImageLink(dataReader["ImageHashName"].ToString());
                                    }
                                }
                                catch (Exception ex)
                                {
                                    string ss = ex.Message;
                                }

                                items.Add(id, new ContentItem()
                                {
                                    title = (string)dataReader["Title"],
                                    host = (string)dataReader["Domain_Host"],
                                    views = (int)dataReader["Views"],
                                    link = (string)dataReader["Link"],
                                    tags = new List<string>() { (string)dataReader["Tag_Id"] },
                                    imageLink = image,
                                    date = DateTime.Parse(dataReader["Date"].ToString())
                                });
                            }
                            else
                            {
                                items[id].tags.Add((string)dataReader["Tag_Id"]);
                            }
                        }
                    }
                }
            }
            return items.OrderByDescending(a=>a.Key).ToDictionary(a=>a.Key,a=>a.Value);
        }

        public static string[] UserTagsFromPgSqlDatabase(string sqlQuery)
        {
            try
            {
                string[] result = new string[2];
                using (NpgsqlConnection cn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["PgContext"].ConnectionString))
                {
                    cn.Open();
                    NpgsqlCommand command = new NpgsqlCommand(sqlQuery, cn);
                    using (NpgsqlDataReader dataReader = command.ExecuteReader())
                    {
                        while (dataReader.Read())
                        {
                            result[0] = dataReader["ObservableTags"].ToString();
                            if(dataReader["ObservableTags"] != null && dataReader["ObservableTags"] != DBNull.Value)
                            {
                                result[1] = dataReader["UnobservableTags"].ToString();
                            }
                        }
                    }
                 }
                return result;
            }
            catch (Exception ex)
            {
                logger.Error("Не удалось выполнить запрос {0} : Message {1}", sqlQuery, ex.Message);
                return null;
            }
        }

        public static bool SqlExecuteNonQuery(string sqlQuery)
        {
            try
            {
                using (NpgsqlConnection cn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["PgContext"].ConnectionString))
                {
                    cn.Open();
                    NpgsqlCommand command = new NpgsqlCommand(sqlQuery, cn);
                    command.ExecuteNonQuery();
                    command.Dispose();
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Error("Не удалось выполнить запрос {0} : Message {1}",sqlQuery, ex.Message);
                return false;
            }
        }
    }
}