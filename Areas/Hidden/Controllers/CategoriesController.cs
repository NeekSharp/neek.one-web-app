﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models;
using NLog;

namespace WebApplication.Areas.Hidden.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CategoriesController : Controller
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Hidden/Categories
        public async Task<ActionResult> Index(int? current)
        {
            List<Categories> categories = new List<Categories>();
            List<Tag> tags = new List<Tag>();
            if (current.HasValue)
            {
                Categories currentCategory = db.Categories.Include("Tags").FirstOrDefault(a => a.Id == current);
                categories = await db.Categories.Where(a => a.Parent.Id == current.Value).ToListAsync();
                tags = currentCategory.Tags.ToList();
            }
            else
            {
                categories = db.Categories.Where(a=>a.Parent==null).ToList();
            }

            ViewBag.AllTags = db.Tags.Where(a => a.Visible && a.ParentTags.Count == 0).ToList();
            ViewBag.Tags = tags;
            ViewBag.Current = current;
            return View(categories);
        }

        // GET: Hidden/Categories/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Categories categories = await db.Categories.FindAsync(id);
            if (categories == null)
            {
                return HttpNotFound();
            }
            return View(categories);
        }

        // GET: Hidden/Categories/Create
        public ActionResult Create(int? parent)
        {
            if (parent.HasValue)
            {
                ViewBag.ParentId = new SelectList(db.Categories.Where(a=>a.Id == parent), "Id", "Name");
            }
            else
            {
                ViewBag.ParentId = new SelectList(db.Categories, "Id", "Name");
            }
            
            return View();
        }

        // POST: Hidden/Categories/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,ParentId")] Categories categories)
        {
            if (ModelState.IsValid)
            {
                db.Categories.Add(categories);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.ParentId = new SelectList(db.Categories, "Id", "Name", categories.ParentId);
            return View(categories);
        }

        public void AddTag(int categoryId,string tag)
        {
            Categories category = db.Categories.Include("Tags").FirstOrDefault(a => a.Id == categoryId);
            if (category != null && !category.Tags.Any(a=>a.Id == tag))
            {
                Tag tagg = db.Tags.Find(tag);
                if (tagg != null)
                {
                    category.Tags.Add(tagg);
                    db.SaveChanges();
                }
                
            }

            Response.Redirect(Request.UrlReferrer.ToString());
        }

        public void RemoveTag(int categoryId, string tag)
        {
            Categories category = db.Categories.Include("Tags").FirstOrDefault(a => a.Id == categoryId);
            if (category != null && category.Tags.Any(a => a.Id == tag))
            {
                Tag tagg = db.Tags.Find(tag);
                if (tagg != null)
                {
                    category.Tags.Remove(tagg);
                    db.SaveChanges();
                }

            }

            Response.Redirect(Request.UrlReferrer.ToString());
        }

        // GET: Hidden/Categories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Categories categories = db.Categories.Include("Tags").FirstOrDefault(a=>a.Id == id);
            if (categories == null)
            {
                return HttpNotFound();
            }
            ViewBag.ParentId = new SelectList(db.Categories, "Id", "Name", categories.ParentId);

            ViewBag.Tags = db.Tags.Where(a => a.Visible == true).ToList();

            return View(categories);
        }

        // POST: Hidden/Categories/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,ParentId")] Categories categories, string[] tags)
        {
            if (ModelState.IsValid)
            {
                if (categories.ParentId == categories.Id) categories.ParentId = null;

                db.Entry(categories).State = EntityState.Modified;
                Categories cat = db.Categories.Include("Tags").FirstOrDefault(a => a.Id == categories.Id);
                List<Tag> existTag = cat.Tags.ToList();
                existTag.ForEach(a => categories.Tags.Remove(a));

                if(tags != null && tags.Count() > 0)
                {
                    var newTags = db.Tags.Where(a => tags.Contains(a.Id)).ToList<Tag>();
                    newTags.ForEach(a => categories.Tags.Add(a));
                }
                await db.SaveChangesAsync();
                
            }
            ViewBag.ParentId = new SelectList(db.Categories, "Id", "Name", categories.ParentId);
            ViewBag.Tags = db.Tags.Where(a=>a.Visible == true).ToList();
            return View(categories);
        }

        // GET: Hidden/Categories/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Categories categories = await db.Categories.FindAsync(id);
            if (categories == null)
            {
                return HttpNotFound();
            }
            return View(categories);
        }

        // POST: Hidden/Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Categories categories = await db.Categories.FindAsync(id);
            db.Categories.Remove(categories);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
