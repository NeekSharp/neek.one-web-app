﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models;

namespace WebApplication.Areas.Hidden.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ContentsController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        // GET: Hidden/Contents
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ByFeed(string url)
        {
            return View(db.Contents.Where(a => a.Feed.Url == url).ToList());
        }
    }
}