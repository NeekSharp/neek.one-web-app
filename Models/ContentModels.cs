﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CommonLibrary;
using WebApplication.Models;
using System.Collections.ObjectModel;

namespace WebApplication.Models
{
    [Table("contents")]
    public class Content
    {
        /*
              id integer NOT NULL DEFAULT nextval('dbo."Contents_Id_seq"'::regclass),
              title character varying(300) NOT NULL DEFAULT ''::character varying,
              text text,
              link character varying(360) NOT NULL DEFAULT ''::character varying,
              views integer NOT NULL DEFAULT 0,
              date timestamp without time zone NOT NULL DEFAULT '-infinity'::timestamp without time zone,
              rating double precision NOT NULL DEFAULT 0,
              block boolean NOT NULL DEFAULT false,
              comment text,
              failed_receiving integer NOT NULL DEFAULT 0,
              post_id_on_site character varying(300) NOT NULL,
              domain_host character varying(90),
              feed_url character varying(350),
              image_name character varying(50),
              language character varying(3) NOT NULL,
        */

        // "Id" serial NOT NULL,
        [ScaffoldColumn(false)]
        [Column("id")]
        public int Id { get; set; }

        // "Title" character varying(300) NOT NULL DEFAULT ''::character varying,
        [Required]
        [Display(Name = "Тема")]
        [Column("title")]
        [StringLength(300)]
        public string Title { get; set; }

        // "Text" text,
        [Display(Name = "Текст")]
        [Column("text")]
        [AllowHtml]
        public string Text { get; set; }


        // "Link" character varying(360) NOT NULL DEFAULT ''::character varying,
        [Required]
        [Url]
        [Index]
        [Column("link")]
        [Display(Name = "Ссылка")]
        [StringLength(360)]
        public string Link { get; set; }

        // "Views" integer NOT NULL DEFAULT 0,
        [Column("views")]
        [Display(Name = "Просмотры")]
        public int Views { get; set; }

        // "Date" timestamp without time zone NOT NULL DEFAULT '-infinity'::timestamp without time zone,
        [Index]
        [Column("date")]
        [Display(Name = "Дата получения")]
        public DateTime Date { get; set; }

        // "Rating" double precision NOT NULL DEFAULT 0,
        [Column("rating")]
        [Display(Name = "Рейтинг")]
        public double Rating { get; set; }

        // "Block" boolean NOT NULL DEFAULT false,
        [Display(Name = "Виден пользователям")]
        [Column("public")]
        public bool Public { get; set; }

        // "Comment" text,
        [Column("comment")]
        public string Comment { get; set; }

        //"FailedReceiving" integer NOT NULL DEFAULT 0,
        [Display(Name = "Неудачи получения контента")]
        [Column("failed_receiving")]
        public int FailedReceiving { get; set; }

        // "PostIdOnSite" character varying(300) NOT NULL,
        [Required]
        [Index(IsUnique = false)]
        [Column("post_id_on_site")]
        [StringLength(300)]
        public string PostIdOnSite { get; set; }

        // "Domain_Host" character varying(90),
        [Display(Name = "Домен")]
        [StringLength(90)]
        [ForeignKey("Domain_Host")]
        public Domain Domain { get; set; }

        [StringLength(90)]
        [Column("domain_host")]
        public string Domain_Host { get; set; }

        // "Feed_Url" character varying(350),
        [ForeignKey("Feed_Url")]
        public Feed Feed { get; set; }

        [StringLength(360)]
        [Column("feed_url")]
        public string Feed_Url { get; set; }

        //"ImagePath" character varying(50),
        [Column("image_name")]
        public string ImageName { get; set; }

        // "Language" character varying(3) NOT NULL,
        [Index(IsUnique = false)]
        [ForeignKey("Language")]
        public Languages TextLanguage { get; set; }

        [Index(IsUnique = false)]
        [Column("language")]
        public string Language { get; set; }

        [Column("likes_count")]
        [Display(Name ="Количество лайков")]
        public int LikesCount { get; set; }

        [Required]
        [Display(Name = "Теги")]
        public virtual ICollection<Tag> Tags { get; set; }

        public virtual ICollection<ApplicationUser> Users { get; set; }

        public virtual ICollection<ApplicationUser> Likers { get; set; }
    }

    [Table("tags")]
    public class Tag
    {
        /*
          id character varying(60) NOT NULL DEFAULT ''::character varying,
          title character varying(100) NOT NULL DEFAULT ''::character varying,
          visible boolean NOT NULL DEFAULT false,
          description character varying(500),
          creation_date timestamp without time zone,
        */

        public Tag()
        {
            Contents = new Collection<Content>();
            Categories = new Collection<Categories>();
            Feeds = new Collection<Feed>();
            ParentTags = new Collection<Tag>();
            ChildTags = new Collection<Tag>();
            Users = new Collection<ApplicationUser>();
        }

        [ScaffoldColumn(true)]
        [StringLength(60)]
        [Display(Name = "Название для uri")]
        [Column("id")]
        [Key]
        public string Id { get; set; }

        [Required]
        [Display(Name = "Имя тега")]
        [Index]
        [StringLength(100)]
        [Column("title")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Показывать пользователю")]
        [Column("visible")]
        public bool Visible { get; set; }

        [Display(Name = "Описание")]
        [StringLength(500)]
        [Column("description")]
        public string Description { get; set; }

        [Display(Name = "Дата создания")]
        [Column("creation_date")]
        public DateTime CreationDate { get; set; }

        public virtual ICollection<Content> Contents { get; set; }
        public virtual ICollection<Categories> Categories { get; set; }
        public virtual ICollection<Feed> Feeds { get; set; }
        public virtual ICollection<Tag> ParentTags { get; set; }
        public virtual ICollection<Tag> ChildTags { get; set; }
        public virtual ICollection<ApplicationUser> Users { get; set; }
    }

    [Table("feeds")]
    public class Feed
    {
        /*
          url character varying(350) NOT NULL DEFAULT ''::character varying,
          title text,
          description character varying(100000),
          last_check_time timestamp without time zone NOT NULL,
          last_update_time timestamp without time zone NOT NULL,
          comment text,
          block boolean NOT NULL DEFAULT false,
          failures integer NOT NULL DEFAULT 0,
          uri_scheme character varying(8) NOT NULL DEFAULT ''::character varying,
          rating double precision NOT NULL DEFAULT 0,
          domain_host character varying(90),
          tag_id character varying(60),
          do_not_update boolean NOT NULL DEFAULT true,
          language character varying(3) NOT NULL DEFAULT 'rus'::character varying,
          auto_publish_content boolean NOT NULL DEFAULT false,
        */

        // "Url" character varying(350) NOT NULL DEFAULT ''::character varying,
        [Key]
        [StringLength(350)]
        [Column("url")]
        public string Url { get; set; }

        // "Title" text
        [Display(Name = "Название")]
        [Column("title")]
        public string Title { get; set; }

        // "Description" character varying(100000),
        [Display(Name = "Описание")]
        [StringLength(100000)]
        [Column("description")]
        public string Description { get; set; }

        // "Language" character varying(3) NOT NULL DEFAULT 'rus'::character varying,
        [Display(Name = "Язык текстов")]
        [ForeignKey("Language")]
        public Languages TextLanguage { get; set; }

        [Index(IsUnique = false)]
        [Column("language")]
        public string Language { get; set; }

        // "LastCheckTime" timestamp without time zone NOT NULL,
        [Display(Name = "Время последней проверки")]
        [Column("last_check_time")]
        public DateTime LastCheckTime { get; set; }

        // "LastUpdateTime" timestamp without time zone NOT NULL,
        [Index]
        [Display(Name = "Время последнего обновления")]
        [Column("last_update_time")]
        public DateTime LastUpdateTime { get; set; }

        // "Domain_Host" character varying(90),
        [ForeignKey("Domain_Host")]
        public virtual Domain Domain { get; set; }

        [StringLength(90)]
        [Column("domain_host")]
        public string Domain_Host { get; set; }

        // "Comment" text,
        [Column("comment")]
        public string Comment { get; set; }

        // "Block" boolean NOT NULL DEFAULT false,
        [Index]
        [Column("block")]
        public bool Block { get; set; }

        // "Failures" integer NOT NULL DEFAULT 0,
        [Display(Name = "Количество неудачный попыток обновления подряд")]
        [Column("failures")]
        public int Failures { get; set; }

        // "UriScheme" character varying(8) NOT NULL DEFAULT ''::character varying,
        [Required]
        [StringLength(8)]
        [Column("uri_scheme")]
        public string UriScheme { get; set; }

        // "Rating" double precision NOT NULL DEFAULT 0,
        [Column("rating")]
        public double Rating { get; set; }

        // "DoNotUpdate" boolean NOT NULL DEFAULT true,
        [Column("do_not_update")]
        public bool DoNotUpdate { get; set; }

        // "Tag_Id" character varying(30),
        [ForeignKey("Tag_Id")]
        public virtual Tag Tag { get; set; }

        [Column("tag_id")]
        public string Tag_Id { get; set; }

        [Column("create_date")]
        public DateTime CreateDate { get; set; }

        public virtual List<Content> Contents { get; set; }
    }

    [Table("domains")]
    public class Domain
    {
        /*
          host character varying(90) NOT NULL DEFAULT ''::character varying,
          comment text,
          block boolean NOT NULL DEFAULT false,
          is_new boolean,
          automatic_update boolean NOT NULL DEFAULT true,
          feed_list_update_time timestamp without time zone,
          last_update_time timestamp without time zone NOT NULL DEFAULT '2016-01-31 17:53:14.425273+03'::timestamp with time zone,
          CONSTRAINT "PK_dbo.Domains" PRIMARY KEY (host)
        */
        [Key]
        [MaxLength(90)]
        [Display(Name = "Имя хоста")]
        [Column("host")]
        public string Host { get; set; }

        public virtual List<Content> Contents { get; set; }

        public virtual List<Feed> Feeds { get; set; }

        [Column("comment")]
        public string Comment { get; set; }

        [Index]
        [Column("block")]
        public bool Block { get; set; }

        [Column("is_new")]
        public bool IsNew { get; set; }

        [Required]
        [Column("automatic_update")]
        public bool AutomaticUpdate { get; set; }

        [Index]
        [Column("last_update_time")]
        public DateTime LastUpdateTime { get; set; }

        [Index]
        [Column("feed_list_update_time")]
        public DateTime LastListUpdateTime { get; set; }

        [Index]
        [Column("creation_date")]
        public DateTime CreationDate { get; set; }
    }

    [Table("categories")]
    public class Categories
    {
        /*
              name character varying(60) NOT NULL,
              id integer NOT NULL DEFAULT nextval('dbo."Categories_Id_seq"'::regclass),
              parent integer,
        */
        public Categories()
        {
            Tags = new Collection<Tag>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public int Id { get; set; }

        [Required]
        [Column("name")]
        [StringLength(60)]
        public string Name { get; set; }

        [Column("parent")]
        public int? ParentId { get; set; }


        public Categories Parent { get; set; }

        public ICollection<Tag> Tags { get; set; }

    }

    [Table("languages")]
    public class Languages
    {
        /*
          id character varying(3) NOT NULL,
          english_name character varying(60),
          local_name character varying(60),
        */
        [Key]
        [StringLength(3)]
        [Column("id")]
        public string Id { get; set; }

        [StringLength(60)]
        [Column("english_name")]
        public string EnglishName { get; set; }

        [Column("local_name")]
        [StringLength(60)]
        public string LocalName { get; set; }

        [Column("russian_name")]
        [StringLength(60)]
        public string RussianName { get; set; }

        [Column("visible")]
        public bool Visible { get; set; }

        public virtual ICollection<Content> Contents { get; set; }
        public virtual ICollection<Feed> Feeds { get; set; }
    }
}