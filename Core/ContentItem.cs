﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Core
{
    public class ContentItem
    {
        public int id { get; set; }
        public string title { get; set; }
        public string host { get; set; }
        public List<string> tags { get; set; }
        public int views { get; set; }
        public string link { get; set; }
        public string imageLink { get; set; }
        public DateTime date { get; set; }
        public string text { get; set; }
        public int likes { get; set; }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
    }
}