﻿using System;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using WebApplication.Core;
using System.Threading.Tasks;

namespace WebApplication.Controllers
{
    [Authorize]
    [ApplicationAuthorize]
    public class MyController : Controller
    {
        private AppCache _cache = null;
        private readonly int _maxContentCount = 0;

        public MyController()
        {
            _maxContentCount = 30;
            _cache = new AppCache();
        }

        [HttpPost]
        public async Task<JsonResult> Newest(string tag, string lang, int skip)
        {
            ContentItem[] items = new ContentItem[0];
            if (tag != null && !Utils.IsCorrectTagName(tag)) return Json(items);
            if (lang.Length > 3) return Json(items);

            

            string userId = User.Identity.GetUserId();
            string cacheKey = null;

            if (!String.IsNullOrWhiteSpace(tag)) 
            {
                // Если тег задан, то нет разницы какой это пользователь, данные по тегу могут быть в общем кеше
                cacheKey = _cache.GenerateKey("Newest", tag, lang, skip);
            }
            else 
            {
                // если тего нет, то надо делать запрос по тегам пользователя, а это другой запрос
                cacheKey = _cache.GenerateKey(userId, "Newest", tag, lang, skip);
            }

            if (_cache.Contains(cacheKey))
            {
                // Берем из кеша
                items = _cache.Get(cacheKey);
            }
            else
            {
                // Делаем запрос к базе и кешируем
                if (!String.IsNullOrWhiteSpace(tag))
                {
                    // Этот же кэш будет использовать при любых других запросах статей тега, в том числе для общих
                    items = await ContentFromDB.TagsContentOrderByTime(tag, lang, skip, _maxContentCount);
                    _cache.Add(cacheKey, items);
                }
                else
                {
                    items = await ContentFromDB.UserFavoritsContentsByTags(userId, skip, _maxContentCount, lang);
                    _cache.Add(cacheKey, items);
                }
            }

            return Json(items);
        }

        [HttpPost]
        public async Task<JsonResult> Today(string tag, string lang, int skip)
        {
            ContentItem[] items = new ContentItem[0];
            if (tag != null && !Utils.IsCorrectTagName(tag)) return Json(items);
            if (lang.Length > 3) return Json(items);


            string userId = User.Identity.GetUserId();
            string cacheKey = null;

            if (!String.IsNullOrWhiteSpace(tag))
            {
                // Если тег задан, то нет разницы какой это пользователь, данные по тегу могут быть в общем кеше
                cacheKey = _cache.GenerateKey("Today", tag, lang, skip);
            }
            else
            {
                // если тего нет, то надо делать запрос по тегам пользователя, а это другой запрос
                cacheKey = _cache.GenerateKey(userId, "Today", tag, lang, skip);
            }

            if (_cache.Contains(cacheKey))
            {
                // Берем из кеша
                items = _cache.Get(cacheKey);
            }
            else
            {
                if (String.IsNullOrWhiteSpace(tag))
                {
                    items = await ContentFromDB.AllContentOrderByRating(skip, _maxContentCount, lang, DateTime.Now.AddDays(-1));
                    _cache.Add(cacheKey, items, 360);
                }
                else {
                    items = await ContentFromDB.TagsContentOrderByRating(tag, lang, skip, _maxContentCount, DateTime.Now.AddDays(-1));
                    _cache.Add(cacheKey, items, 360);
                }
            }
            return Json(items);
        }

        [HttpPost]
        public async Task<JsonResult> Weekly(string tag, string lang, int skip)
        {
            ContentItem[] items = new ContentItem[0];
            if (tag != null && !Utils.IsCorrectTagName(tag)) return Json(items);
            if (lang.Length > 3) return Json(items);

            

            string userId = User.Identity.GetUserId();
            string cacheKey = null;

            if (!String.IsNullOrWhiteSpace(tag))
            {
                // Если тег задан, то нет разницы какой это пользователь, данные по тегу могут быть в общем кеше
                cacheKey = _cache.GenerateKey("Weekly", tag, lang, skip);
            }
            else
            {
                // если тего нет, то надо делать запрос по тегам пользователя, а это другой запрос
                cacheKey = _cache.GenerateKey(userId, "Weekly", tag, lang, skip);
            }

            if (_cache.Contains(cacheKey))
            {
                // Берем из кеша
                items = _cache.Get(cacheKey);
            }
            else
            {
                if (String.IsNullOrWhiteSpace(tag))
                {
                    items = await ContentFromDB.AllContentOrderByRating(skip, _maxContentCount, lang, DateTime.Now.AddDays(-7));
                    _cache.Add(cacheKey, items, 360);
                }
                else {

                    items = await ContentFromDB.TagsContentOrderByRating(tag, lang, skip, _maxContentCount, DateTime.Now.AddDays(-7));
                    _cache.Add(cacheKey, items, 360);
                }
            }
            return Json(items);
        }

        [HttpPost]
        public async Task<JsonResult> Monthy(string tag, string lang, int skip)
        {
            ContentItem[] items = new ContentItem[0];
            if (tag != null && !Utils.IsCorrectTagName(tag)) return Json(items);
            if (lang.Length > 3) return Json(items);

            

            string userId = User.Identity.GetUserId();
            string cacheKey = null;

            if (!String.IsNullOrWhiteSpace(tag))
            {
                // Если тег задан, то нет разницы какой это пользователь, данные по тегу могут быть в общем кеше
                cacheKey = _cache.GenerateKey("Monthy", tag, lang, skip);
            }
            else
            {
                // если тего нет, то надо делать запрос по тегам пользователя, а это другой запрос
                cacheKey = _cache.GenerateKey(userId, "Monthy", tag, lang, skip);
            }

            if (_cache.Contains(cacheKey))
            {
                // Берем из кеша
                items = _cache.Get(cacheKey);
            }
            else
            {
                if (String.IsNullOrWhiteSpace(tag))
                {
                    items = await ContentFromDB.AllContentOrderByRating(skip, _maxContentCount, lang, DateTime.Now.AddDays(-30));
                    _cache.Add(cacheKey, items, 360);
                }
                else {
                    items = await ContentFromDB.TagsContentOrderByRating(tag, lang, skip, _maxContentCount, DateTime.Now.AddDays(-30));
                    _cache.Add(cacheKey, items, 360);
                }
            }
            return Json(items);
        }

    }
}