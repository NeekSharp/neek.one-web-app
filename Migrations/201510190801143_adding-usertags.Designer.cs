// <auto-generated />
namespace WebApplication.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class addingusertags : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addingusertags));
        
        string IMigrationMetadata.Id
        {
            get { return "201510190801143_adding-usertags"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
