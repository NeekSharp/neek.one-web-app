namespace WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _3 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Contents", new[] { "PostIdOnSite" });
            AlterColumn("dbo.Contents", "PostIdOnSite", c => c.String(nullable: false, maxLength: 300));
            CreateIndex("dbo.Contents", "PostIdOnSite");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Contents", new[] { "PostIdOnSite" });
            AlterColumn("dbo.Contents", "PostIdOnSite", c => c.String(nullable: false, maxLength: 200));
            CreateIndex("dbo.Contents", "PostIdOnSite");
        }
    }
}
