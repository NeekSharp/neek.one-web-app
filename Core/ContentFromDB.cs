﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Npgsql;
using System.Data;
using System.Threading.Tasks;
using WebApplication.Models;

namespace WebApplication.Core
{
    public class ContentFromDB
    {
        public class UserData
        {
            public HashSet<string> Tags { get; set; }
            public HashSet<BookmarkItem> Bookmarks { get; set; }
            public HashSet<int> Likes { get; set; }
        }

        public static async Task<ContentItem[]> TagsContentOrderByTime(string tag, string lang, int skip, int take)
        {
            if (!Utils.IsCorrectTagName(tag)) return null;

            NpgsqlCommand command = new NpgsqlCommand("SELECT c.id, c.title, c.text, c.link, c.views, c.date, c.rating, c.domain_host, c.image_name, array_agg(DISTINCT t2.tag_id), c.likes_count " +
                                                        "FROM dbo.contents c INNER JOIN dbo.tag_contents t ON c.id = t.content_id LEFT JOIN dbo.tag_contents t2 ON t2.content_id = t.content_id " +
                                                        "WHERE t.tag_id IN (SELECT child_tag_id " +
                                                          "FROM dbo.tags_relations tr INNER JOIN dbo.tags ta ON tr.child_tag_id = ta.id " +
                                                          "WHERE parent_tag_id = @Tag " +
                                                          "AND ta.visible = true " +
                                                          "UNION  " +
                                                          "SELECT @Tag) " +
                                                        "AND c.Language = @Lang " +
                                                        "AND c.public = true " +
                                                        "GROUP BY c.id  " +
                                                        "ORDER BY c.id DESC " +
                                                        "OFFSET @Skip LIMIT @Take;");

            command.Parameters.AddWithValue("@Tag", tag);
            command.Parameters.AddWithValue("@Take", take);
            command.Parameters.AddWithValue("@Skip", skip);
            command.Parameters.AddWithValue("@Lang", lang);

            return DataToContentItems(await SendQuery(command));
        }

        public static async Task<ContentItem[]> AllContentOrderByTime(int skip, int take, string lang)
        {

            NpgsqlCommand command = new NpgsqlCommand("SELECT c.id, c.title, c.text, c.link, c.views, c.date, c.rating, c.domain_host, c.image_name, array_agg(DISTINCT t2.tag_id), c.likes_count " +
                                                        "FROM dbo.contents c INNER JOIN dbo.tag_contents t ON c.id = t.content_id LEFT JOIN dbo.tag_contents t2 ON t2.content_id = t.content_id  " +
                                                        "WHERE t2.tag_id IN (SELECT ta.id " +
                                                            "FROM dbo.tags ta " +
                                                            "WHERE ta.visible = true) " +
                                                        "AND c.Language = @Lang " +
                                                        "AND c.public = true " +
                                                        "GROUP BY c.id  " +
                                                        "ORDER BY c.id DESC  " +
                                                        "OFFSET @Skip LIMIT @Take;");

            command.Parameters.AddWithValue("@Take", take);
            command.Parameters.AddWithValue("@Skip", skip);
            command.Parameters.AddWithValue("@Lang", lang);

            return DataToContentItems(await SendQuery(command));
        }

        public static async Task<ContentItem[]> UserBookmarks(string userId, int skip, int take)
        {
            NpgsqlCommand command = new NpgsqlCommand("SELECT c.id, c.title, c.text, c.link, c.views, c.date, c.rating, c.domain_host, c.image_name, array_agg(DISTINCT t.tag_id), c.likes_count " +
                                                        "FROM dbo.contents c INNER JOIN dbo.tag_contents t ON c.id = t.content_id " +
                                                        "WHERE c.id IN (	SELECT uc.\"Content_Id\" " +
                                                                "FROM dbo.\"ApplicationUserContents\" uc " +
                                                                "WHERE uc.\"ApplicationUser_Id\" = @User " +
                                                                "ORDER BY uc.\"Content_Id\" DESC " +
                                                                "OFFSET @Skip LIMIT @Take) " +
                                                        "GROUP BY c.id;");

            command.Parameters.AddWithValue("@User", userId);
            command.Parameters.AddWithValue("@Take", take);
            command.Parameters.AddWithValue("@Skip", skip);

            return DataToContentItems(await SendQuery(command));
        }

        public static async Task<ContentItem[]> UserFavoritsContentsByTags(string userId, int skip, int take, string lang)
        {
            NpgsqlCommand command = new NpgsqlCommand("SELECT c.id, c.title, c.text, c.link, c.views, c.date, c.rating, c.domain_host, c.image_name, array_agg(DISTINCT t2.tag_id), c.likes_count  " +
                                                        "FROM dbo.contents c INNER JOIN dbo.tag_contents t ON c.id = t.content_id LEFT JOIN dbo.tag_contents t2 ON t2.content_id = t.content_id " +
                                                        "WHERE t.tag_id IN ( " +
                                                            "SELECT ut.tag_id  " +
                                                            "FROM dbo.user_tags ut INNER JOIN dbo.tags t ON ut.tag_id = t.id  " +
                                                            "WHERE ut.user_id = @User AND t.visible = true ) " +
                                                        "AND c.Language = @Lang " +
                                                        "AND c.public = true " +
                                                        "GROUP BY c.id " +
                                                        "ORDER BY c.id DESC  " +
                                                        "OFFSET @Skip LIMIT @Take;");

            command.Parameters.AddWithValue("@User", userId);
            command.Parameters.AddWithValue("@Skip", skip);
            command.Parameters.AddWithValue("@Take", take);
            command.Parameters.AddWithValue("@Lang", lang);


            return DataToContentItems(await SendQuery(command));
        }

        public static async Task<ContentItem[]> UserBookmarksByTag(string tag, string userId, int skip, int take)
        {
            NpgsqlCommand command = new NpgsqlCommand("SELECT c.id, c.title, c.text, c.link, c.views, c.date, c.rating, c.domain_host, c.image_name, array_agg(DISTINCT t2.tag_id), c.likes_count " +
                                                        "FROM dbo.contents c INNER JOIN dbo.tag_contents t ON c.id = t.content_id LEFT JOIN dbo.tag_contents t2 ON t2.content_id = t.content_id " +
                                                        "WHERE t.tag_id IN(SELECT child_tag_id " +
                                                          "FROM dbo.tags_relations tr INNER JOIN dbo.tags ta ON tr.child_tag_id = ta.id " +
                                                          "WHERE parent_tag_id = @Tag " +
                                                          "AND ta.visible = true " +
                                                          "UNION " +
                                                          "SELECT @Tag) " +
                                                        "AND c.id IN(SELECT uc.\"Content_Id\" " +
                                                                "FROM dbo.\"ApplicationUserContents\" uc " +
                                                                "WHERE uc.\"ApplicationUser_Id\" = @User) " +
                                                        "GROUP BY c.id " +
                                                        "ORDER BY c.id DESC " +
                                                        "OFFSET @Skip LIMIT @Take; ");

            command.Parameters.AddWithValue("@User", userId);
            command.Parameters.AddWithValue("@Take", take);
            command.Parameters.AddWithValue("@Skip", skip);
            command.Parameters.AddWithValue("@Tag", tag);

            return DataToContentItems(await SendQuery(command));
        }

        public static async Task<ContentItem[]> AllContentOrderByRating(int skip, int take, string lang, DateTime olderThenDate)
        {

            NpgsqlCommand command = new NpgsqlCommand("SELECT c.id, c.title, c.text, c.link, c.views, c.date, c.rating, c.domain_host, c.image_name, array_agg( DISTINCT t2.tag_id), c.likes_count "+
                                                        "FROM dbo.contents c INNER JOIN dbo.tag_contents t ON c.id = t.content_id LEFT JOIN dbo.tag_contents t2 ON t2.content_id = t.content_id " +
                                                        "WHERE t2.tag_id IN(SELECT ta.id " +
                                                            "FROM dbo.tags ta " +
                                                            "WHERE ta.visible = true) " +
                                                        "AND c.Date >= @OlderThen " +
                                                        "AND c.Language = @Lang " +
                                                        "AND c.public = true " +
                                                        "GROUP BY c.id " +
                                                        "ORDER BY c.rating DESC " +
                                                        "OFFSET @Skip LIMIT @Take; ");

            command.Parameters.AddWithValue("@Take", take);
            command.Parameters.AddWithValue("@Skip", skip);
            command.Parameters.AddWithValue("@Lang", lang);
            command.Parameters.AddWithValue("@OlderThen", olderThenDate.Date);

            return DataToContentItems(await SendQuery(command));
        }

        public static async Task<ContentItem[]> TagsContentOrderByRating(string tag, string lang, int skip, int take, DateTime olderThenDate)
        {
            if (!Utils.IsCorrectTagName(tag)) return null;

            NpgsqlCommand command = new NpgsqlCommand("SELECT c.id, c.title, c.text, c.link, c.views, c.date, c.rating, c.domain_host, c.image_name, array_agg(DISTINCT t2.tag_id), c.likes_count " +
                                                        "FROM dbo.contents c INNER JOIN dbo.tag_contents t ON c.id = t.content_id LEFT JOIN dbo.tag_contents t2 ON t2.content_id = t.content_id " +
                                                        "WHERE t.tag_id IN(SELECT child_tag_id " +
                                                          "FROM dbo.tags_relations tr INNER JOIN dbo.tags ta ON tr.child_tag_id = ta.id " +
                                                          "WHERE parent_tag_id = @Tag " +
                                                          "AND ta.visible = true " +
                                                          "UNION " +
                                                          "SELECT @Tag) " +
                                                        "AND c.Language = @Lang " +
                                                        "AND c.public = true " +
                                                        "AND c.date >= @OlderThen " +
                                                        "GROUP BY c.id " +
                                                        "ORDER BY c.rating DESC " +
                                                        "OFFSET @Skip LIMIT @Take; ");

            command.Parameters.AddWithValue("@Tag", tag);
            command.Parameters.AddWithValue("@Take", take);
            command.Parameters.AddWithValue("@Skip", skip);
            command.Parameters.AddWithValue("@Lang", lang);
            command.Parameters.AddWithValue("@OlderThen", olderThenDate.Date);

            return DataToContentItems(await SendQuery(command));
        }

        public static async Task<UserData> UsersData(string userId)
        {
            NpgsqlCommand command = new NpgsqlCommand("SELECT \"Content_Id\", tag_id  FROM dbo.\"ApplicationUserContents\" WHERE \"ApplicationUser_Id\" = @User;");

            command.Parameters.AddWithValue("@User", userId);
            DataTable rawtable = await SendQuery(command);

            HashSet<BookmarkItem> bookmarks = new HashSet<BookmarkItem>();
            
            

            if (rawtable != null && rawtable.Rows.Count > 0)
            {
                foreach (DataRow item in rawtable.Rows)
                {
                    string tag = item.Field<string>("tag_id");
                    int id = item.Field<int>("content_id");

                    BookmarkItem bookmarkItem = new BookmarkItem(id, tag);
                    if (!bookmarks.Contains(bookmarkItem))
                        bookmarks.Add(bookmarkItem);
                }
            }

            UserData data = new UserData();
            data.Bookmarks = bookmarks;
            data.Tags = await GetUserFavoritTags(userId);
            data.Likes = await GetUserLikes(userId);

            return data;
        }

        private static async Task<HashSet<string>> GetUserFavoritTags(string userId)
        {
            HashSet<string> tags = new HashSet<string>();

            NpgsqlCommand command2 = new NpgsqlCommand("SELECT ut.tag_id " +
                                                       "FROM dbo.user_tags ut INNER JOIN dbo.tags t ON ut.tag_id = t.id " +
                                                       "WHERE ut.user_id = @User AND t.visible = true; ");

            command2.Parameters.AddWithValue("@User", userId);
            DataTable rawtable2 = await SendQuery(command2);

            if (rawtable2 != null && rawtable2.Rows.Count > 0)
            {
                foreach (DataRow item in rawtable2.Rows)
                {
                    string id = item.Field<string>("tag_id");

                    if (!tags.Contains(id))
                        tags.Add(id);
                }
            }

            return tags;
        }

        private static async Task<HashSet<int>> GetUserLikes(string userId)
        {
            HashSet<int> tags = new HashSet<int>();

            NpgsqlCommand command2 = new NpgsqlCommand("SELECT content_id FROM dbo.likes WHERE user_id = @User; ");

            command2.Parameters.AddWithValue("@User", userId);
            DataTable rawtable2 = await SendQuery(command2);

            if (rawtable2 != null && rawtable2.Rows.Count > 0)
            {
                foreach (DataRow item in rawtable2.Rows)
                {
                    int id = item.Field<int>("content_id");

                    if (!tags.Contains(id))
                        tags.Add(id);
                }
            }

            return tags;
        }

        private static ContentItem[] DataToContentItems(DataTable data)
        {
            if (data == null || data.Rows.Count == 0) return null;

            List<ContentItem> contents = new List<ContentItem>();
            foreach (DataRow row in data.Rows)
            {
                int id = row.Field<int>("id");

                contents.Add(new ContentItem()
                {
                    id = id,
                    title = row.Field<string>("title"),
                    host = row.Field<string>("domain_host"),
                    date = row.Field<DateTime>("date"),
                    link = row.Field<string>("link"),
                    imageLink = row.Field<string>("image_name"),
                    tags = new List<string>(row.Field<string[]>("array_agg")),
                    views = row.Field<int>("views"),
                    text = row.Field<string>("text"),
                    likes = row.Field<int>("likes_count")
                });

            }

            return contents.ToArray();
        }

        private static async Task<DataTable> SendQuery(NpgsqlCommand command)
        {
            DataTable result = null;
            using (PgDatabase database = new PgDatabase())
            {
                if (database.Open())
                {
                    result = await database.SelectAsync(command);
                }
            }

            return result;
        }

        private static async Task<int> ExecuteNonQuery(NpgsqlCommand command)
        {
            int result = 0;
            using (PgDatabase database = new PgDatabase())
            {
                if (database.Open())
                {
                    result = await database.ExecuteNonQueryAsync(command);
                }
            }
            return result;
        }

        public static async Task<int> AddBookmark(string user, string tag, int content)
        {
            NpgsqlCommand command = new NpgsqlCommand("INSERT INTO dbo.\"ApplicationUserContents\"(\"ApplicationUser_Id\", \"Content_Id\",tag_id) VALUES (@User, @Content, @Tag);");
            command.Parameters.AddWithValue("@User", user);
            command.Parameters.AddWithValue("@Content", content);
            command.Parameters.AddWithValue("@Tag", tag);

            return await ExecuteNonQuery(command);
        }

        public static async Task<int> RemoveBookmark(string user, int content)
        {
            NpgsqlCommand command = new NpgsqlCommand("DELETE FROM dbo.\"ApplicationUserContents\" WHERE \"ApplicationUser_Id\" = @User AND \"Content_Id\" = @Content;");
            command.Parameters.AddWithValue("@User", user);
            command.Parameters.AddWithValue("@Content", content);

            return await ExecuteNonQuery(command);
        }

        public static async Task<int> AddLike(string user, int content)
        {
            NpgsqlCommand command = new NpgsqlCommand("INSERT INTO dbo.likes (user_id, content_id) VALUES (@User, @Content);");
            command.Parameters.AddWithValue("@User", user);
            command.Parameters.AddWithValue("@Content", content);

            return await ExecuteNonQuery(command);
        }

        public static async Task<int> RemoveLike(string user, int content)
        {
            NpgsqlCommand command = new NpgsqlCommand("DELETE FROM dbo.likes WHERE user_id = @User AND content_id = @Content;");
            command.Parameters.AddWithValue("@User", user);
            command.Parameters.AddWithValue("@Content", content);

            return await ExecuteNonQuery(command);
        }

        public static async Task AddView(int content)
        {
            NpgsqlCommand command = new NpgsqlCommand("UPDATE dbo.contents SET views=views+1 WHERE id = @Id;");
            command.Parameters.AddWithValue("@Id", content);
            await ExecuteNonQuery(command);

            await UpdateRating(content, 1, 0, 0);
        }

        public static async Task<int> UpdateRating(int content, int views,int likes,int bookmarks)
        {
            NpgsqlCommand command = new NpgsqlCommand("UPDATE dbo.contents a " +
                                                           "SET rating = subquery.frating " +
                                                           "FROM (SELECT id,((cast(likes_count+bookmarks_count+views+@Views+@Likes+@Bookmarks as double precision)/(cast(likes_count+bookmarks_count+views+3+@Views+@Likes+@Bookmarks as double precision)))*((likes_count+@Likes)*1.0000000+(bookmarks_count+@Bookmarks)*0.90000000+(views+@Views)*0.00100000))+((3.0000000/(3.0000000+likes_count+bookmarks_count+views+@Views+@Likes+@Bookmarks))) frating " +
                                                            "FROM dbo.contents " +
                                                            "WHERE id = @Content) as subquery " +
                                                        "WHERE a.id = subquery.id;");

            command.Parameters.AddWithValue("@Content", content);
            command.Parameters.AddWithValue("@Views", views);
            command.Parameters.AddWithValue("@Likes", likes);
            command.Parameters.AddWithValue("@Bookmarks", bookmarks);

            return await ExecuteNonQuery(command);
        }
    }
}