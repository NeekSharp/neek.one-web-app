﻿using CommonLibrary.Helpers;
using NLog;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace WebApplication.Core
{
    interface IDatabase
    {
        bool Open();

        void Close();

        void Dispose();

        Task<DataTable> SelectAsync(DbCommand command);

        Task<int> ExecuteNonQueryAsync(DbCommand command);
    }

    public class PgDatabase : IDatabase, IDisposable
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private readonly string _connectionString = null;
        private NpgsqlConnection _connection = null;
        public Exception Exception { get; set; }

        public PgDatabase()
        {
            _connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["PgContext"].ConnectionString;
        }

        public bool Open()
        {
            try
            {
                _connection = new NpgsqlConnection(_connectionString);
                _connection.Open();
                return true;
            }
            catch (Exception ex)
            {
                logger.Fatal(ex, "Не удалось открыть соединение в базой данных" + ex.GetaAllMessages(), null);
            }

            return false;
        }

        public void Close()
        {
            if (_connection.State != ConnectionState.Closed)
            {
                _connection.Close();
            }
        }

        public void Dispose()
        {
            Close();
        }

        public async Task<DataTable> SelectAsync(DbCommand command)
        {
            DataTable result = new DataTable();

            command.Connection = _connection;
            try
            {
                using (DbDataReader reader = await command.ExecuteReaderAsync())
                {
                    if (reader.HasRows)
                    {
                        result.Load(reader);
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Не удалось выполнить запрос: " + ex.GetaAllMessages(), null);
                
            }

            return null;
        }

        public async Task<int> ExecuteNonQueryAsync(DbCommand command)
        {
            int changes = 0;
            command.Connection = _connection;
            try
            {
                changes = await command.ExecuteNonQueryAsync();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Не удалось выполнить запрос: " + ex.GetaAllMessages(), null);
            }

            return changes;
        }

    }
}