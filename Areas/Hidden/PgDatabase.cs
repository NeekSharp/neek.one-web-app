﻿using NLog;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace WebApplication.Areas.Hidden
{
    class PgDatabase : IDisposable
    {
        enum QueryStatus
        {
            Error = -1,
            NoChanges = 0
        }

        private static Logger logger = LogManager.GetCurrentClassLogger();
        private readonly string _connectionString = null;
        private NpgsqlConnection _connection = null;
        public Exception Exception { get; set; }

        public PgDatabase()
        {
            _connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["PgContext"].ConnectionString;
        }

        public bool Open()
        {
            try
            {
                _connection = new NpgsqlConnection(_connectionString);
                _connection.Open();

                logger.Debug("PgDatabase состояние: {0}", _connection.State.ToString());

                return true;
            }
            catch (Exception ex)
            {
                logger.Debug("Не удалось открыть соединение в базой данных");
                logger.Error(ex, "Не удалось открыть соединение в базой данных", null);
            }
            return false;
        }

        public void Close()
        {
            if (_connection.State != ConnectionState.Closed)
            {
                _connection.Close();
                logger.Debug("PgDatabase состояние: {0}", _connection.State.ToString());
            }
        }

        private async Task<DataTable> SelectAsync(NpgsqlCommand command)
        {
            DataTable result = new DataTable();
            try
            {
                command.Connection = _connection;

                using (var reader = await command.ExecuteReaderAsync())
                {
                    if (reader.HasRows)
                    {
                        result.Load(reader);
                        logger.Debug("SelectAsync удачное выполнение запросв");
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                Exception = ex;
                logger.Debug("SelectAsync Не удалось выполнить запрос: { 0}", command.CommandText);
                logger.Error(ex, "Не удалось выполнить запрос: {0}", command.CommandText);
            }
            return null;
        }

        public async Task<int> ExecuteNonQueryAsync(NpgsqlCommand command)
        {
            int changes = 0;
            try
            {
                command.Connection = _connection;
                changes = await command.ExecuteNonQueryAsync();
            }
            catch (Exception ex)
            {
                Exception = ex;
                logger.Error(ex, "Не удалось выполнить запрос: {0}", command.CommandText);
            }
            return changes;
        }

        public void Dispose()
        {
            Close();
        }
    }
}