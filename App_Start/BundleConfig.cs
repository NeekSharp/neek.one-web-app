﻿using System.Web;
using System.Web.Optimization;

namespace WebApplication
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/main").Include(
                    "~/Scripts/masonry.pkgd.min.js",
                    "~/Scripts/imageloaded.min.js",
                    "~/Scripts/slideout.min.js",
                    "~/Scripts/infinite-scroll.js",
                    "~/Scripts/typeahead.jquery.min.js",
                      "~/Scripts/main.js"));

            bundles.Add(new ScriptBundle("~/bundles/my").Include(
                   "~/Scripts/masonry.pkgd.min.js",
                   "~/Scripts/imageloaded.min.js",
                   "~/Scripts/infinite-scroll.js",
                     "~/Scripts/slideout.min.js",
                     "~/Scripts/my.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/normalize.css",
                        "~/Content/skeleton.css"
                      ));
        }
    }
}
