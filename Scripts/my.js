﻿
$(document).ready(function () {
    var showImage = false;
    var minId = 2147483647;
    var isEnd = false;
    var $container = $(".grid");
    var mobileWidth = 1100;


    /*
        Inicialize
    */
    checkWidth();
    $(window).resize(null, checkWidth);

    // Masonry
    $container.masonry({
        columnWidth: 300,
        isFitWidth: true,
        itemSelector: '.grid-item',
        //gutter: 10,
        transitionDuration: 0
    });

    /*
        Infinite Scroll
    */
    infiniteScroll({
        distance: 50,
        callback: function (done) {
            // 1. fetch data from the server
            // 2. insert it into the document
            // 3. call done when we are done
            getContent(true);
            done();
        }
    });

    /*
        Events
    */

    $('.main').on('click', '.post .info .delete', function (e) {
        removeFromFavorits(e.target.getAttribute("data-value"));
        $(e.target).closest('.grid-item').remove();
        $container.masonry();
    });

    $('.user-tags').on('click', 'a', function (e) {
        getContent(e.target.getAttribute("data-value"), false);
    });

    /*
        Get Data
    */


    function getContent(tagId,nextpage) {
        if (isEnd) return;

        toggleSpinner();

        if (nextpage == false) {
            minId = 2147483647;
        }

        var headers = {};
        var form = $('#__AjaxAntiForgeryForm');
        headers['__RequestVerificationToken'] = $('input[name="__RequestVerificationToken"]', form).val();

        $.ajax({
            url: '/My/Favorites',
            dataType: "json",
            type: "POST",
            headers: headers,
            data: JSON.stringify({
                "tagId": tagId,
                "minId": minId
            }),
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: true,
            dataFilter: function (data) {
                if ($.isEmptyObject(data)) {
                    isEnd = true;
                }
                return data;
            },
            success: function (data) {
                var jsondata = JSON.parse(data);
                processingData(jsondata, nextpage);
                if (tagId == null) {
                    getAllTags(jsondata);
                }
                toggleSpinner();
            },
            error: function (xhr) {

                toggleSpinner();
            }
        });
    }

    function removeFromFavorits(contentId) {
        $.ajax({
            url: '/My/RemoveFromFavorites',
            dataType: "json",
            type: "POST",
            data: JSON.stringify({
                "Id": contentId
            }),
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: true,
            success: function (data) {

            },
            error: function (xhr) {

            }
        });
    }



    /*
        Processing Data
    */
    function processingData(data, nextpage) {
        var posts = [];

        var imageStatus = false;
        var image = null;

        for (var property in data) {

            if (imageStatus == true) {
                image = data[property].ImageLink;
            }

            posts.push(new Post(data[property].Title, data[property].Link, data[property].Host, image, data[property].Tags, property, Date(data[property].Date)));

            if (parseInt(property) < minId) {
                minId = parseInt(property);
            }
        }
        displayItems(posts, nextpage);
    }

    function displayItems(posts, nextpage) {
        var elems = [];

        if (!nextpage) {
            $container.masonry('remove', $container.children());
            $container.children().remove();
        }

        for (var i = 0; i < posts.length; i++) {

            var elem = posts[i].CreateElement();
            elems.push(elem);
        };

        var $elems = $(elems).css({ opacity: 0 });
        //$elems;
        $container.append($elems);

        $container.imagesLoaded(function () {
            $container.masonry('appended', $elems);
            $elems.animate({ opacity: 1 });
            $container.masonry();
        });
    }

    /*
        Functions
    */
    function toggleSpinner() {
        $('.spinner').toggleClass('hidden');
    }

    function getAllTags(data) {
        var usertags = {};
        for (property in data) {
            data[property].Tags.forEach(function (item, i, arr) {
                usertags[item] = tags[item];
            });
        }
        console.log(usertags);
        createTagsMenu(usertags);
    }

    function createTagsMenu(usertags) {
        var $menu = $('.user-tags');
        for (property in usertags) {
            $menu.append('<li><a href="#" data-value="' + property + '">' + usertags[property] + '</a></li>');
        }
    }

    function checkWidth() {
        if (document.body.clientWidth < mobileWidth) {
            var slideout = new Slideout({
                'panel': document.getElementById('bodypanel'),
                'menu': document.getElementById('asidemenu'),
                'padding': 300,
                'tolerance': 0
            });

            document.querySelector('#sidemenu').addEventListener('click', function () {
                slideout.toggle();
            });

            slideout.on('open', function () {
                $('#sidemenu').addClass('slidemenu-contrast');
            });

            slideout.on('close', function () {
                $('#sidemenu').removeClass('slidemenu-contrast');
            });
        }
    }

    /*
        Classes
    */
    function Post(_title, _link, _host, _image, _tags, _id, _date) {
        this.title = _title;
        this.link = _link;
        this.host = _host;
        this.image = _image;
        this.tags = _tags;
        this.id = _id;
        this.date = new Date(_date);


        this.CreateElement = function () {

            var block = document.createElement('div');
            block.className = "post-block";
            block.className += " u-cf";


            var postTitle = document.createElement('div');
            postTitle.className = "post-title";
            postTitle.appendChild(document.createTextNode(this.title));

            var postHost = document.createElement('div');
            postHost.className = "host";
            postHost.appendChild(document.createTextNode(this.date.toLocaleDateString() + " / " + this.host));

            var saveButton = document.createElement('div');
            saveButton.className = "delete";
            saveButton.setAttribute('data-value', this.id);
            saveButton.appendChild(document.createTextNode("Удалить"));

            var info = document.createElement('div');
            info.className = "info";

            info.appendChild(postHost);
            info.appendChild(saveButton)
            block.appendChild(postTitle).appendChild(info);


            var postImage = document.createElement('div');
            postImage.className = "post-img";
            if (this.image != null) {
                var imageLink = document.createElement('img');
                imageLink.setAttribute('src', this.image);
                postImage.appendChild(imageLink);
            }

            var taglist = document.createElement('div');
            taglist.className = "post-tags";
            taglist.className += " u-cf";

            for (var i = 0; i < this.tags.length; i++) {
                var item = document.createElement('span');
                item.className = "tag";
                item.setAttribute('data-value', this.tags[i]);

                item.appendChild(document.createTextNode(tags[this.tags[i]]));
                taglist.appendChild(item);
            }


            var gridItem = document.createElement('div');
            gridItem.className = "grid-item";

            var post = document.createElement('div');
            post.className = "post";

            if (this.image != null) {
                post.appendChild(postImage);
            }

            post.appendChild(block);
            post.appendChild(document.createElement('hr'));
            post.appendChild(taglist);
            //post.appendChild(document.createElement('hr'));
            //post.appendChild(otherInfo);

            gridItem.appendChild(post);

            //console.log("all created");
            return gridItem;
        }
    }

    Share = {
        vkontakte: function (purl, ptitle, pimg, text) {
            url = 'http://vkontakte.ru/share.php?';
            url += 'url=' + encodeURIComponent(purl);
            url += '&title=' + encodeURIComponent(ptitle);
            url += '&description=' + encodeURIComponent(text);
            url += '&image=' + encodeURIComponent(pimg);
            url += '&noparse=true';
            Share.popup(url);
        },
        odnoklassniki: function (purl, text) {
            url = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
            url += '&st.comments=' + encodeURIComponent(text);
            url += '&st._surl=' + encodeURIComponent(purl);
            Share.popup(url);
        },
        facebook: function (purl, ptitle, pimg, text) {
            url = 'http://www.facebook.com/sharer.php?s=100';
            url += '&p[title]=' + encodeURIComponent(ptitle);
            url += '&p[summary]=' + encodeURIComponent(text);
            url += '&p[url]=' + encodeURIComponent(purl);
            url += '&p[images][0]=' + encodeURIComponent(pimg);
            Share.popup(url);
        },
        twitter: function (purl, ptitle) {
            url = 'http://twitter.com/share?';
            url += 'text=' + encodeURIComponent(ptitle);
            url += '&url=' + encodeURIComponent(purl);
            url += '&counturl=' + encodeURIComponent(purl);
            Share.popup(url);
        },
        mailru: function (purl, ptitle, pimg, text) {
            url = 'http://connect.mail.ru/share?';
            url += 'url=' + encodeURIComponent(purl);
            url += '&title=' + encodeURIComponent(ptitle);
            url += '&description=' + encodeURIComponent(text);
            url += '&imageurl=' + encodeURIComponent(pimg);
            Share.popup(url)
        },
        google: function(purl){
            url = 'https://plus.google.com/share?';
            url += 'url=' + encodeURIComponent(purl);
        },

        popup: function (url) {
            window.open(url, '', 'toolbar=0,status=0,width=626,height=436');
        }
    };

    $('#menu').load('/Home/isAuth', function () {
        getContent(null, false);
    });

    

});