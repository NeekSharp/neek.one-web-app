﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;

namespace WebApplication.Core
{
    public class SendMail
    {
        public static async Task Send(string mailTo, string subject, string message)
        {
            string login = ConfigurationManager.AppSettings["email-login"];
            string password = ConfigurationManager.AppSettings["email-password"];

            MailAddress from = new MailAddress("Hello@neek.one", "NEEK.ONE");
            MailAddress to = new MailAddress(mailTo);

            using (SmtpClient smtp = new SmtpClient("smtp.yandex.ru", 587))
            {
                MailMessage mail = new MailMessage(from, to);
                mail.Subject = subject;
                mail.Body = "<!doctype html><html><body>" + message + "</body></html>";
                mail.IsBodyHtml = true;

                smtp.Credentials = new NetworkCredential(login, password);
                smtp.EnableSsl = true;

                await smtp.SendMailAsync(mail);
            }

        }
    }
}