namespace WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingusertags : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "UserTags", c => c.String(maxLength: 5000));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "UserTags");
        }
    }
}
