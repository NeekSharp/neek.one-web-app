﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Areas.Hidden.Models
{
    public class TagsSortParams
    {
        public string Title { get; set; }
        public bool Visible { get; set; }
        public bool HasParent { get; set; }
        public bool HasChildren { get; set; }
        public DateTime CreationDate { get; set; }
    }

    public class FeedSortParams
    {
        public string Domain { get; set; }
        public DateTime CreationDateStart { get; set; }
        public DateTime CreationDateEnd { get; set; }
        public bool DoNotUpdate { get; set; }
    }

    public class DomiansSortParams
    {
        public string Name { get; set; }
        public bool AutoUpdate { get; set; }
        public bool IsNew { get; set; }
        public DateTime AddingTime { get; set; }
    }
}