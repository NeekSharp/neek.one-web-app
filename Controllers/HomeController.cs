﻿using System.Web.Mvc;

namespace WebApplication.Controllers
{
    public class HomeController : Controller
    {
        [OutputCache(Location = System.Web.UI.OutputCacheLocation.Any, Duration = 360)]
        public ActionResult Index()
        {
            return View();
        }
    }

}