﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication.Areas.Hidden.Controllers
{
    [Authorize(Roles = "Admin")]
    public class MainController : Controller
    {
        // GET: Hidden/Main
        public ActionResult Index()
        {
            return View();
        }
    }
}