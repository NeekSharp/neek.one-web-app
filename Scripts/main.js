﻿$(document).ready(function () {
    includeTarget = ".tag-include";
    excludeTarget = ".tag-exclude";
    var showImage = false;
    var minId = 2147483647;
    var isEnd = false;
    var $container = $(".grid");
    var useLocalStorage = true;
    var mobileWidth = 1100;

    /*
        Inicialize
    */
    $('#menu').load('/Home/isAuth', function () {
        if (storageAvailable('localStorage')) {
            if (typeof observableTags != 'undefined' && observableTags != null && observableTags != '') {
                var re = /'/gi;
                localStorage.clear();
                localStorage.setItem(includeTarget, observableTags.replace(re, ''));
                if (typeof unobservableTags != 'undefined') {
                    localStorage.setItem(excludeTarget, unobservableTags.replace(re, ''));
                }
            }
            GetTagLocalStorage();
        }
        else {
            useLocalStorage = false;
            GetTagFromCookie();
        }
    });

    tryEnableMobleMenu();
    $(window).resize(null, tryEnableMobleMenu);

    // Masonry
     $container.masonry({
            columnWidth: 300,
            isFitWidth: true,
            itemSelector: '.grid-item',
            transitionDuration: 0
    });

    /*
        Infinite Scroll
    */

     infiniteScroll({
         distance: 50,
         callback: function (done) {
             if ($container.css('display') != 'none') {
                 getContent(true);
             }
            done();
         }
     });
    /* 
        Autocomplete
    */
    var substringMatcher = function (strs) {
        return function findMatches(q, cb) {
            var matches, substringRegex;

            // an array that will be populated with substring matches
            matches = [];

            // regex used to determine if a string contains the substring `q`
            substrRegex = new RegExp(q, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array

            $.each(strs, function (i, str) {
                if (substrRegex.test(str)) {
                    matches.push(str);
                }
            });

            cb(matches);
        };
    };

    $('#the-basics .typeahead').typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    },
    {
        name: 'tags',
        source: substringMatcher(tags),
        limit:15
    });

    $('.typeahead').bind('typeahead:select typeahead:autocomplete', function (ev, suggestion) {
        var tag;
        for (param in tags) {
            if (tags[param] == suggestion) {
                tag = param;
                break;
            }
        }

        if (!$(excludeTarget).find("[data-value='" + tag + "']")[0]) {
            addTag(includeTarget, tag, true);
        }
        
        $(this).typeahead('val', '').typeahead('close');
    });

    $('#all-tags-list').on('click', '.tag-in-list', function () {
        var $this = $(this);
        var tag = $this.attr('data-value');

        if ($this.hasClass('observable'))
        {
            removeTag(includeTarget, tag);
            $(includeTarget).find("[data-value='" + tag + "']").remove();
            $this.removeClass('observable');
        }
        else if ($this.hasClass('unobservable'))
        {

        }
        else {
            if (!$(includeTarget).find("[data-value='" + tag + "']")[0]) {
                addTag(includeTarget, tag, true);
                $this.addClass('observable');
            }
        }
        
    });


    /*
        Events
    */

    $('.typeahead').bind('typeahead:idle', function (ev, suggestion) {
        $(this).typeahead('val', '').typeahead('close');
    });

    $('.tag-include, .tag-exclude').on('click', '.tag', function (e) {
        var $this = $(this);
        var parentClass = e.target.parentElement.className.split(' ')[0];

        removeTag('.' + parentClass, $this.attr('data-value'));
        $this.remove();
    });

    $('#showtaglist').click(function () {
        if ($container.css('display') == 'none') {
            $container.css('display', 'block');
            $('#all-tags-list').addClass('hidden');
        } else {
            $container.css('display', 'none');
            $('#all-tags-list').removeClass('hidden');
            showAllTags();
        }
        
    });

    $('#get-content').click(function () {
        // remove all grid-item elements
        $container.children('div').remove();

        
        if ($container.css('display')=='none') {
            $('#all-tags-list').addClass('hidden');
            $($container).css('display', 'block');
        }

        isEnd = false;
        getContent(false);
        
        if (!$('.first-message').hasClass('hidden')) {
            $('.first-message').addClass('hidden');
        }
        
        if (document.body.clientWidth < mobileWidth) {
            $(".sidebar").trigger("sidebar:toggle");
        }



    });

    
        /*
            Tooltips
        */

        if (!isTouch()) {

            //$('<p class="tooltip"></p>')
            //.text('')
            //.appendTo('body')
            //.fadeIn('fast').hide();

            $('body').on('mouseenter', '*[data-tooltip]', function () {
                var text = $(this).attr('data-tooltip');
                //$('.tooltip').show().html(text).fadeIn('fast');

                $('<p class="tooltip"></p>')
                .text('')
                .appendTo('body')
                .html(text)
                .delay(400)
                .fadeIn('slow');

            });

            $('body').on('mouseleave', '*[data-tooltip]', function () {
                //$('.tooltip').hide();
                $('.tooltip').remove();
            });

            $('body').on('mousemove', '*[data-tooltip]', function (e) {
                var mousex = e.pageX + 10;
                var mousey = e.pageY - 30;

               $('.tooltip').css({ top: mousey, left: mousex })
            });
        }


    $('.main').on('click', '.post-tags .tag', function (e) {
        var $this = $(this);
        var datavalue = $this.attr('data-value');
        $('span[data-value=' + datavalue + ']').closest('.grid-item').remove();
        addTag(excludeTarget, datavalue, true);
        removeTag(includeTarget, datavalue);
        $this.remove();
        $container.masonry();
    });

    $('.main').on('click', '.post .info .save', function () {
        addToFavorits($(this).attr('data-value'));
        $(this).text('В избранном');
    });

    $('.content-options-images input').is(':checked');

    $('#add-website-bottom').on('click', function () {
        $('.add-new-website-form').toggleClass('hidden');
    });

    $('.add-new-website-form button').on('click', function () {
        $.ajax({
            method: "POST",
            url: "/home/OfferNewWebsite",
            data: { str: $('.add-new-website-form input').val() }
        })
          .done(function () {
              $('.add-new-website-form input').val('');
          });
    });

    /*
        Get Data
    */


    function getContent(nextpage) {
        if (isEnd) return;

        var including = getIncludingTags();
        var excluding = getExcludingTags();

        if (nextpage == false) {
            minId = 2147483647;
        }

        if (including == null || including.length < 1) {
            return;
        }

        toggleSpinner();
        
        var headers = {};
        var form = $('#__AjaxAntiForgeryForm');
        headers['__RequestVerificationToken'] = $('input[name="__RequestVerificationToken"]', form).val();

        $.ajax({
            url: '/Home/Content',
            dataType: "json",
            type: "POST",
            headers: headers,
            data: JSON.stringify({
                "observable": including,
                "unobservable": excluding,
                "lastId": minId
            }),
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: true,
            dataFilter: function (data) {
                if($.isEmptyObject(data)){
                    isEnd = true;
                }
                return data;
            },
            success: function (data) {
                processingData(JSON.parse(data), nextpage);
                toggleSpinner();
            },
            error: function (xhr) {
                toggleSpinner();
            }
        });
    }

    function addToFavorits(contentId) {
        $.ajax({
            url: '/My/AddToFavorites',
            dataType: "json",
            type: "POST",
            data: JSON.stringify({
                "Id": contentId
            }),
            contentType: 'application/json; charset=utf-8',
            async: true,
            processData: false,
            cache: true,
            error: function (xhr) {
                var status = JSON.parse(xhr.getResponseHeader('X-Responded-JSON'))['status'];
                if (status == 401 || status == 302) {
                    location = 'Account/Login';
                }
            }
        });
    }

    function toggleSpinner() {
        $('.spinner').toggleClass('hidden');
    }

    function getIncludingTags() {
        var array = [];
        $('.tag-include').children().each(function (index, value) {
            var val = value.getAttribute('data-value');
            
            if (val) {
                array.push(val);
            }
        });
        return array;
    }

    function getExcludingTags() {
        var array = [];
        $('.tag-exclude').children().each(function (index, value) {
            var val = value.getAttribute('data-value');
            if (val) {
                array.push(val);
            }
        });
        return array;
    }

    /*
        Processing Data
    */
    function processingData(data, nextpage) {
        var posts = [];

        var imageStatus = document.getElementById("showimage").checked;
        var image = null;
       
        for (var property in data) {
            
            if (imageStatus == true) {
                image = data[property].ImageLink;
            }

            posts.push(new Post(data[property].Title, data[property].Link, data[property].Host, image, data[property].Tags, property, data[property].Date));
            
            if (parseInt(property) < minId) {
                minId = parseInt(property);
            }
        }
        displayItems(posts, nextpage);
    }

    function displayItems(posts, nextpage) {
        var elems = [];

        if (!nextpage) {
            $container.masonry('remove', $container.children());
            $container.children().remove();
        }
   

        for (var i = 0; i < posts.length; i++) {     
            var elem = posts[i].CreateElement();
            elems.push(elem);
        };

        var $elems = $(elems).css({ opacity: 0 });
        //$elems;
        $container.append($elems);
        
        $container.imagesLoaded(function () {
            $container.masonry('appended', $elems);
            $elems.animate({ opacity: 1 });
            $container.masonry();
        });
    }

    /*
        Functions
    */
    function storageAvailable(type) {
        try {
            var storage = window[type],
                x = '__storage_test__';
            storage.setItem(x, x);
            storage.removeItem(x);
            return true;
        }
        catch (e) {
            return false;
        }
    }

    function addTag(target, tag, changesCookie) {
        if (target == excludeTarget) {
            var inIncludeTags = $(includeTarget).find("[data-value='" + tag + "']")[0];
            if (inIncludeTags!=null) {
                inIncludeTags.remove();
            }
        }

        var $target = $(target);
        if (!$target.find("[data-value='" + tag + "']")[0]) {
            $target.append('<span class="tag" data-tooltip="Исключить из списка" data-value="' + tag + '">' + tags[tag] + '&nbsp;&nbsp;x</span>');
            $target.find('.tags-desc')[0].style.display = "none";
        }

        if (changesCookie) {
            if (useLocalStorage)
                addTagLocalStorage(target, tag);
            else
                addTagCookie(target, tag);
        }
    }

    function removeTag(target, tagname) {
        var $target = $(target);

        if (useLocalStorage)
            removeTagLocalStorage(target, tagname);
        else
            removeTagCookie(target, tagname);

        if ($target.find('.tag').length == 0) {
            $target.find('.tags-desc')[0].style.display = "inline";
        }
    }

    function addTagCookie(target, value) {
        if (docCookies.hasItem(target) && docCookies.getItem(target) != null) {
            var cookie = docCookies.getItem(target).split(',');
            cookie.push(value);
            docCookies.setItem(target, cookie);
        } else {
            docCookies.setItem(target, value);
        }
    }

    function removeTagCookie(target, value) {
        if (docCookies.hasItem(target) && docCookies.getItem(target) != null) {
            var cookie = docCookies.getItem(target).split(',');
            var i = cookie.indexOf(value);
            cookie.splice(i, 1);
            docCookies.setItem(target, cookie);
        }
    }

    function GetTagFromCookie() {
        if (docCookies.hasItem(includeTarget) && docCookies.getItem(includeTarget) != null) {
            var tagInclude = docCookies.getItem(includeTarget).split(',');

            for (var i = 0; i < tagInclude.length; i++) {
                addTag(includeTarget, tagInclude[i], false);
            }

            if (docCookies.hasItem(excludeTarget) && docCookies.getItem(excludeTarget) != null) {
                var tagExclude = docCookies.getItem(excludeTarget).split(',');
                for (var i = 0; i < tagExclude.length; i++) {
                    addTag(excludeTarget, tagExclude[i], false);
                }
            }
        }
        
        getContent(false);
    }

    function GetTagLocalStorage() {
        if (localStorage.getItem(includeTarget) !== null && localStorage.getItem(includeTarget) != '') {
            var tagInclude = localStorage.getItem(includeTarget).split(',');

            for (var i = 0; i < tagInclude.length; i++) {
                addTag(includeTarget, tagInclude[i], false);
            }

            if (localStorage.getItem(excludeTarget) !== null && localStorage.getItem(excludeTarget) != '') {
                var tagExclude = localStorage.getItem(excludeTarget).split(',');
                for (var i = 0; i < tagExclude.length; i++) {
                    addTag(excludeTarget, tagExclude[i], false);
                }
            }
            getContent(false);
        } else {
            $('.first-message').removeClass('hidden');
        }
    }

    function addTagLocalStorage(target, value) {
        if (localStorage.getItem(target) != null && localStorage.getItem(target) != '') {
            var localitems = localStorage.getItem(target).split(',');

            if (localitems.indexOf(value) == -1) {
                localitems.push(value);
                localStorage.setItem(target, localitems);
            }
        } else {
            localStorage.setItem(target, value);
        }
    }

    function removeTagLocalStorage(target, value) {
        if (localStorage.getItem(target) != null) {
            var localitems = localStorage.getItem(target).split(',');
            var i = localitems.indexOf(value);
            //console.log(i);
            localitems.splice(i, 1);
            localStorage.setItem(target, localitems);
        }
    }


    function tryEnableMobleMenu() {
        if (document.body.clientWidth < mobileWidth) {
            var slideout = new Slideout({
                'panel': document.getElementById('bodypanel'),
                'menu': document.getElementById('asidemenu'),
                'padding': 300,
                'tolerance': 0
            });

            document.querySelector('#sidemenu').addEventListener('click', function () {
                slideout.toggle();
            });

            slideout.on('open', function () {
                $('#sidemenu').addClass('slidemenu-contrast');
            });

            slideout.on('close', function () {
                $('#sidemenu').removeClass('slidemenu-contrast');
            });
        }
    }

    function isTouch() {
        return ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;
    }
    
    function showAllTags() {
        
        var tagsList = $('#all-tags-list');
        tagsList.children('div').remove();

        var sortedTags = sortProperties(tags);
        var observable = getIncludingTags();
        var unobservable = getExcludingTags();

        for (var i = 0; i < sortedTags.length; i++) {
            var item = document.createElement('div');
            item.className = "tag-in-list";
            if (observable.indexOf(sortedTags[i][0]) != -1) {
                item.className += ' observable'
            } else {
                if (unobservable.indexOf(sortedTags[i][0]) != -1) {
                    item.className += ' unobservable'
                }
            }

            item.setAttribute('data-value', sortedTags[i][0]);
            item.appendChild(document.createTextNode(sortedTags[i][1]));
            tagsList.append(item);
        }
    }
    
    function sortProperties(obj) {
        // convert object into array
        var sortable = [];
        for (var key in obj)
            if (obj.hasOwnProperty(key))
                sortable.push([key, obj[key]]); // each item is an array in format [key, value]

        // sort items by value
        sortable.sort(function (a, b) {
            var x = a[1].toLowerCase(),
                y = b[1].toLowerCase();
            return x < y ? -1 : x > y ? 1 : 0;
        });
        return sortable; // array in format [ [ key1, val1 ], [ key2, val2 ], ... ]
    }

    /*
        Classes
    */
    function Post(_title, _link, _host, _image, _tags, _id, _date) {
        this.title = _title;
        this.link = _link;
        this.host = _host;
        this.image = _image;
        this.tags = _tags;
        this.id = _id;
        this.date = new Date(_date);


        this.CreateElement = function () {
            
            //console.log("start");
            //title

            //              <div class="post-block u-cf">
            //                  <div class="post-title">
            //                      Очередь событий в карточной игре + основы Angular
            //                  </div>
            //                  <div class="host">
            //                      habrahabr.ru
            //                  </div>
            //              </div>

            
            var block = document.createElement('div');
            block.className = "post-block";
            block.className += " u-cf";

            
            var postTitle = document.createElement('div');
            var postlink = document.createElement('a');
            postlink.setAttribute('href', 'http://' + this.link);
            postlink.setAttribute('target', '_blank');
            postTitle.className = "post-title";
            postlink.appendChild(document.createTextNode(this.title));
            postTitle.appendChild(postlink);

            var postHost = document.createElement('div');
            postHost.className = "host";
            postHost.appendChild(document.createTextNode(this.date.toLocaleDateString() + " / " + this.host));

            
            var saveButton = document.createElement('div');
            saveButton.className = "save";
            saveButton.setAttribute('data-value', this.id);
            saveButton.appendChild(document.createTextNode("В избранное"));

            
            var info = document.createElement('div');
            info.className = "info";

            info.appendChild(postHost);
            info.appendChild(saveButton)
            block.appendChild(postTitle).appendChild(info);
            //block.appendChild(postTitle).appendChild(saveButton);

            //console.log("title created " + this.title);
            //image


            //<div class="post-img">
            //    <img src="~/Content/img/Diablo.png" />
            //</div>
            
            var postImage = document.createElement('div');
            postImage.className = "post-img";

            if (this.image != null) {
                var imageLink = document.createElement('img');
                imageLink.setAttribute('src', this.image);
                postImage.appendChild(imageLink);
            }

            //console.log("image created");
            // Tags

            //<div class="post-tags u-cf">
            //    <span class="tag" data-value="photo">фотография</span>
            //    <span class="tag" data-value="health">здоровье</span>
            //    <span class="tag" data-value="photo">фотография</span>
            //</div>
           
            var taglist = document.createElement('div');
            taglist.className = "post-tags";
            taglist.className += " u-cf";

            for (var i = 0; i < this.tags.length; i++) {
                var item = document.createElement('span');
                item.className = "tag";
                item.setAttribute('data-value', this.tags[i]);
                item.setAttribute('data-tooltip', 'Исключить тему');

                item.appendChild(document.createTextNode(tags[this.tags[i]]));
                taglist.appendChild(item);
            }

            //console.log("tags created");
            //Other

            //                      <div class="post-block u-cf">
            //                        <div class="info">
            //                          Просмотров:
            //                        </div>

            //                        <div class="likes">
            //                            лайк
            //                        </div>
            //                    </div>

            //var otherInfo = document.createElement('div');
            //otherInfo.className = "post-block";
            //otherInfo.className += " u-cf";


            //console.log("other created");
            //main
            
            var gridItem = document.createElement('div');
            gridItem.className = "grid-item";

            var post = document.createElement('div');
            post.className = "post";

            if (this.image != null) {
                post.appendChild(postImage);
            }

            post.appendChild(block);
            post.appendChild(document.createElement('hr'));
            post.appendChild(taglist);
            
            gridItem.appendChild(post);

            //console.log("all created");
            return gridItem;
        }
    }

    Share = {
        vkontakte: function (purl, ptitle, pimg, text) {
            url = 'http://vkontakte.ru/share.php?';
            url += 'url=' + encodeURIComponent(purl);
            url += '&title=' + encodeURIComponent(ptitle);
            url += '&description=' + encodeURIComponent(text);
            url += '&image=' + encodeURIComponent(pimg);
            url += '&noparse=true';
            Share.popup(url);
        },
        odnoklassniki: function (purl, text) {
            url = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
            url += '&st.comments=' + encodeURIComponent(text);
            url += '&st._surl=' + encodeURIComponent(purl);
            Share.popup(url);
        },
        facebook: function (purl, ptitle, pimg, text) {
            url = 'http://www.facebook.com/sharer.php?s=100';
            url += '&p[title]=' + encodeURIComponent(ptitle);
            url += '&p[summary]=' + encodeURIComponent(text);
            url += '&p[url]=' + encodeURIComponent(purl);
            url += '&p[images][0]=' + encodeURIComponent(pimg);
            Share.popup(url);
        },
        twitter: function (purl, ptitle) {
            url = 'http://twitter.com/share?';
            url += 'text=' + encodeURIComponent(ptitle);
            url += '&url=' + encodeURIComponent(purl);
            url += '&counturl=' + encodeURIComponent(purl);
            Share.popup(url);
        },
        mailru: function (purl, ptitle, pimg, text) {
            url = 'http://connect.mail.ru/share?';
            url += 'url=' + encodeURIComponent(purl);
            url += '&title=' + encodeURIComponent(ptitle);
            url += '&description=' + encodeURIComponent(text);
            url += '&imageurl=' + encodeURIComponent(pimg);
            Share.popup(url)
        },
        google: function (purl) {
            url = 'https://plus.google.com/share?';
            url += 'url=' + encodeURIComponent(purl);
        },

        popup: function (url) {
            window.open(url, '', 'toolbar=0,status=0,width=626,height=436');
        }
    };
});