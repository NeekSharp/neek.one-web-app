namespace WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingusertags1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "ObservableTags", c => c.String(maxLength: 5000));
            AddColumn("dbo.AspNetUsers", "UnobservableTags", c => c.String(maxLength: 5000));
            DropColumn("dbo.AspNetUsers", "UserTags");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "UserTags", c => c.String(maxLength: 5000));
            DropColumn("dbo.AspNetUsers", "UnobservableTags");
            DropColumn("dbo.AspNetUsers", "ObservableTags");
        }
    }
}
