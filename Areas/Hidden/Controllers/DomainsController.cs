﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication.Models;
using WebApplication.Areas.Hidden.Models;

namespace WebApplication.Areas.Hidden.Controllers
{
    [Authorize(Roles = "Admin")]
    public class DomainsController : Controller
    {
        
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create([Bind(Include = "Host,Block,Comment,IsNew,AutomaticUpdate,LastUpdateTime,LastListUpdateTime")]Domain domain)
        {
            DateTime now = DateTime.Now;
            domain.LastListUpdateTime = now;
            domain.LastUpdateTime = now;
            domain.CreationDate = now;

            if (ModelState.IsValid)
            {
                ApplicationDbContext db = new ApplicationDbContext();
                db.Domains.Add(domain);
                db.SaveChanges();
            }
            else
            {
                return View(domain);
            }

            return View();
        }

        [HttpGet]
        public ActionResult Index()
        {
            ApplicationDbContext db = new ApplicationDbContext();

            List<Domain> domains = db.Domains.Where(a=>a.CreationDate == DateTime.Now).ToList();
            ViewBag.Domains = domains;

            return View(new DomiansSortParams()
            {
                Name = String.Empty,
                AddingTime = DateTime.Now,
                AutoUpdate = true,
                IsNew = false
            });
        }

        [HttpPost]
        public ActionResult Index([Bind(Include = "Name,IsNew,AutoUpdate,AddingTime")]DomiansSortParams parameters)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var query = db.Domains.Where(a => a.IsNew == parameters.IsNew && a.AutomaticUpdate == parameters.AutoUpdate);
            if (!String.IsNullOrWhiteSpace(parameters.Name))
            {
                query.Where(a => a.Host.Contains(parameters.Name));
            }

            if (parameters.AddingTime != null)
            {
                query.Where(a => a.CreationDate == parameters.AddingTime);
            }

            ViewBag.Domains = query.ToList();

            return View(parameters);
        }
        
        public bool ChangeDomainsStatus(string domain,bool autoupdate)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            Domain d = db.Domains.Find(domain);
            if (d == null) return false;

            d.AutomaticUpdate = autoupdate;

            db.SaveChanges();
            return true;
        }
    }
}