﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApplication.Core;

namespace WebApplication.Controllers
{
    [Authorize]
    public class BookmarksController : Controller
    {
        [HttpPost]
        public async Task<JsonResult> Index(string tag, int skip)
        {
            string userId = User.Identity.GetUserId();
            ContentItem[] items = new ContentItem[0];
            if (!String.IsNullOrWhiteSpace(tag))
            {
                if (!Utils.IsCorrectTagName(tag)) return Json(items);
                items = await ContentFromDB.UserBookmarksByTag(tag, userId, skip, 40);
            }
            else
            {
                items = await ContentFromDB.UserBookmarks(userId, skip, 40);
            }

            return Json(items);
        }

    }
}