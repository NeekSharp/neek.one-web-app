﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using System.Web.Mvc;
using CommonLibrary.Helpers;

namespace WebApplication.Core
{
    public class AppCache
    {
        private MemoryCache _memoryCache = MemoryCache.Default;

        public bool Add(string key, ContentItem[] value, int seconds = 120)
        {
            return _memoryCache.Add(key, value, DateTime.Now.AddSeconds(seconds));
        }

        public ContentItem[] Get(string key)
        {
            return (ContentItem[])_memoryCache.Get(key);
        }

        public bool Contains(string key)
        {
            return _memoryCache.Contains(key);
        }

        public string GenerateKey(string method, string tag, string lang, int skip)
        {
            return String.Concat(
                    method, 
                    String.IsNullOrWhiteSpace(tag) ? String.Empty : tag, 
                    lang, 
                    skip.ToString())
                .MD5();
        }

        public string GenerateKey(string user, string method, string tag, string lang, int skip)
        {
            return String.Concat(
                    user,
                    method,
                    String.IsNullOrWhiteSpace(tag) ? String.Empty : tag,
                    lang,
                    skip.ToString())
                .MD5();
        }
    }
}