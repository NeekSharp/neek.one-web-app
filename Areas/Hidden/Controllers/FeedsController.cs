﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication.Areas.Hidden.Models;
using WebApplication.Models;

namespace WebApplication.Areas.Hidden.Controllers
{
    [Authorize(Roles = "Admin")]
    public class FeedsController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        // GET: Hidden/Feeds
        public ActionResult Index()
        {
            DateTime last = DateTime.Now.AddDays(-2);
            List<Feed> feeds = db.Feeds.Where(a => a.CreateDate >= last && a.DoNotUpdate == false).ToList();
            ViewBag.Feeds = feeds;

            return View(new FeedSortParams()
            {
                Domain = null,
                CreationDateStart = last,
                CreationDateEnd = DateTime.Now,
                DoNotUpdate = false
            });

        }

        [HttpPost]
        public ActionResult Index([Bind(Include = "Domain,CreationDateStart,CreationDateEnd,DoNotUpdate")]FeedSortParams param)
        {
            var query = db.Feeds.Where(a => a.DoNotUpdate == param.DoNotUpdate);

            if (!String.IsNullOrWhiteSpace(param.Domain))
            {
                query = query.Where(a => a.Domain.Host == param.Domain.ToLower());
            }

            if (param.CreationDateStart != null)
            {
                query = query.Where(a => a.CreateDate >= param.CreationDateStart);
            }

            if (param.CreationDateEnd != null)
            {
                query = query.Where(a => a.CreateDate <= param.CreationDateEnd);
            }

            List<Feed> feeds = query.ToList();
            ViewBag.Feeds = feeds;

            return View(param);
        }

        public ActionResult ByTag(string id)
        {

            List<Feed> feeds = db.Feeds.Where(a => a.Tag.Id == id).ToList();
            return View(feeds);
        }

        public ActionResult ByDomain(string host)
        {
            List<Feed> feeds = db.Feeds.Where(a => a.Domain.Host == host).ToList();
            return View(feeds);
        }

        public ActionResult Contents(string url)
        {
            return View(db.Contents.Where(a => a.Feed.Url == url).ToList());
        }

        // GET: Hidden/Feeds/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Hidden/Feeds/Create
        public ActionResult Create()
        {
            ViewBag.Tags = db.Tags.Where(a => a.Visible == true && a.Feeds.Any(b => b.DoNotUpdate == false && a.ParentTags.Count == 0)).ToList();
            ViewBag.Langs = db.Languages.ToList();
            return View();
        }

        [HttpPost]
        public ActionResult Create(string url, string title, string language, bool donotupdate, string tag)
        {
            ViewBag.Tags = db.Tags.Where(a => a.Visible == true && a.Feeds.Any(b => b.DoNotUpdate == false && a.ParentTags.Count == 0)).ToList();
            ViewBag.Langs = db.Languages.ToList();

            if (String.IsNullOrWhiteSpace(url) || String.IsNullOrWhiteSpace(title) || String.IsNullOrWhiteSpace(language) || String.IsNullOrWhiteSpace(tag))
            {
                ViewBag.Message = "Все параметры должны быть заданы";
                return View();
            }

            Uri uri = null;
            if (!Uri.TryCreate(url, UriKind.Absolute, out uri))
            {
                ViewBag.Message = "Невозможно создать Uri";
                return View();
            }

            DateTime now = DateTime.Now;
            Feed newFeed = new Feed();

            newFeed.UriScheme = uri.Scheme;
            newFeed.Url = uri.Host + uri.PathAndQuery;
            newFeed.Title = title;
            newFeed.Language = language;
            newFeed.DoNotUpdate = donotupdate;

            newFeed.LastCheckTime = now;
            newFeed.LastUpdateTime = now;
            newFeed.Rating = 0;
            newFeed.CreateDate = now;
            newFeed.Failures = 0;

            Domain domain = db.Domains.Find(uri.Host);
            if (domain == null)
            {
                ViewBag.Message = String.Format("Домена {0} нет в базе", uri.Host);
                return View();
            }

            Tag existTag = null;

            if (!String.IsNullOrWhiteSpace(tag))
            {
                existTag = db.Tags.FirstOrDefault(a => a.Id == tag);
                if (existTag == null)
                {
                    existTag = db.Tags.Add(new Tag()
                    {
                        Id = tag,
                        CreationDate = now,
                        Title = newFeed.Title,
                        Visible = true
                    });
                }
            }
            else
            {
                return View();
            }

            newFeed.Domain = domain;
            newFeed.Tag = existTag;
            newFeed.TextLanguage = db.Languages.Find(language);

            db.Feeds.Add(newFeed);
            db.SaveChanges();

            ViewBag.Message = String.Format("Источник {0} Успешно добавлен.", uri.AbsoluteUri);
            return View();
        }

        // GET: Hidden/Feeds/Edit/5
        public ActionResult Edit(string url)
        {
            Feed feed = db.Feeds.Find(url);
            ViewBag.Tag = feed.Tag;
            ViewBag.Tags = db.Tags.Where(a => a.Visible == true);
            ViewBag.Languages = db.Languages.ToList();// select new SelectListItem { Text = lang.RussianName, Value = lang.Id };
            return View(feed);
        }

        // POST: Hidden/Feeds/Edit/5
        [HttpPost]
        public ActionResult Edit([Bind(Include = "Url,Title,Description,Language,Comment,Block,Failures,Rating,DoNotUpdate,Tag_Id")] Feed feed)
        {

            Feed uFeed = db.Feeds.Find(feed.Url);
            uFeed.Title = feed.Title;
            uFeed.Language = feed.Language;
            uFeed.Description = feed.Description;
            uFeed.Comment = feed.Comment;
            uFeed.Block = feed.Block;
            uFeed.Failures = feed.Failures;
            uFeed.Rating = feed.Rating;
            uFeed.Tag_Id = feed.Tag_Id;
            uFeed.Tag = db.Tags.Find(feed.Tag_Id);
            uFeed.DoNotUpdate = feed.DoNotUpdate;
            db.SaveChanges();

            ViewBag.Tag = uFeed.Tag;
            ViewBag.Tags = db.Tags.Where(a => a.Visible == true);
            ViewBag.Languages = db.Languages.ToList();
            return View(feed);
        }

        // GET: Hidden/Feeds/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Hidden/Feeds/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public bool ChangeUpdateStatus(string url,bool doNotUpdate)
        {
            Feed feed = db.Feeds.FirstOrDefault(a => a.Url == url);
            if(feed != null)
            {
                feed.DoNotUpdate = doNotUpdate;
                db.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
