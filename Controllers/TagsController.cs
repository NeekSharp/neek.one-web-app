﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using WebApplication.Core;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using WebApplication.Models;
using System.Data.Entity;
using System.Threading;

namespace WebApplication.Controllers
{
    public class TagsController : Controller
    {
        private AppCache _cache = null;
        private readonly int _maxContentCount = 0;

        public TagsController()
        {
            _maxContentCount = 30;
            _cache = new AppCache();
        }

        // POST : Newest
        [HttpPost]
        public async Task<JsonResult> Newest(string tag,string lang, int skip)
        {
            ContentItem[] items = new ContentItem[0];
            if (!String.IsNullOrWhiteSpace(tag) && !Utils.IsCorrectTagName(tag)) return Json(items);
            if (lang.Length > 3) return Json(items);

            string cacheKey = _cache.GenerateKey("Newest", tag, lang, skip);

            if (_cache.Contains(cacheKey))
            {
                items = _cache.Get(cacheKey);
            }
            else
            {
                if (String.IsNullOrWhiteSpace(tag))
                {
                    items = await ContentFromDB.AllContentOrderByTime(skip, _maxContentCount, lang);
                    _cache.Add(cacheKey, items, 360);
                }
                else {
                    items = await ContentFromDB.TagsContentOrderByTime(tag, lang, skip, _maxContentCount);
                    _cache.Add(cacheKey, items, 360);
                }
            }

            return Json(items);
        }

        [HttpPost]
        public async Task<JsonResult> Today(string tag, string lang, int skip)
        {
            ContentItem[] items = new ContentItem[0];
            if (!String.IsNullOrWhiteSpace(tag) && !Utils.IsCorrectTagName(tag)) return Json(items);
            if (lang.Length > 3) return Json(items);

            string cacheKey = _cache.GenerateKey("Today", tag, lang, skip);

            if (_cache.Contains(cacheKey))
            {
                items = _cache.Get(cacheKey);
            }
            else
            {
                if (String.IsNullOrWhiteSpace(tag))
                {
                    items = await ContentFromDB.AllContentOrderByRating(skip, _maxContentCount, lang, DateTime.Now.AddDays(-1));
                    _cache.Add(cacheKey, items, 360);
                }
                else {
                    items = await ContentFromDB.TagsContentOrderByRating(tag, lang, skip, _maxContentCount, DateTime.Now.AddDays(-1));
                    _cache.Add(cacheKey, items, 360);
                }
            }
            return Json(items);
        }

        [HttpPost]
        public async Task<JsonResult> Weekly(string tag, string lang, int skip)
        {
            ContentItem[] items = new ContentItem[0];
            if (!String.IsNullOrWhiteSpace(tag) && !Utils.IsCorrectTagName(tag)) return Json(items);
            if (lang.Length > 3) return Json(items);

            string cacheKey = _cache.GenerateKey("Weekly", tag, lang, skip);

            if (_cache.Contains(cacheKey))
            {
                items = _cache.Get(cacheKey);
            }
            else
            {
                if (String.IsNullOrWhiteSpace(tag))
                {
                    items = await ContentFromDB.AllContentOrderByRating(skip, _maxContentCount, lang, DateTime.Now.AddDays(-7));
                    _cache.Add(cacheKey, items, 360);
                }
                else {
                    items = await ContentFromDB.TagsContentOrderByRating(tag, lang, skip, _maxContentCount, DateTime.Now.AddDays(-7));
                    _cache.Add(cacheKey, items, 360);
                }
            }
            return Json(items);
        }

        [HttpPost]
        public async Task<JsonResult> Monthy(string tag, string lang, int skip)
        {
            ContentItem[] items = new ContentItem[0];
            if (!String.IsNullOrWhiteSpace(tag) && !Utils.IsCorrectTagName(tag)) return Json(items);
            if (lang.Length > 3) return Json(items);

            string cacheKey = _cache.GenerateKey("Monthy", tag, lang, skip);

            if (_cache.Contains(cacheKey))
            {
                items = _cache.Get(cacheKey);
            }
            else
            {
                if (String.IsNullOrWhiteSpace(tag))
                {
                    items = await ContentFromDB.AllContentOrderByRating(skip, _maxContentCount, lang, DateTime.Now.AddDays(-30));
                    _cache.Add(cacheKey, items, 360);
                }
                else {
                   
                    items = await ContentFromDB.TagsContentOrderByRating(tag, lang, skip, _maxContentCount, DateTime.Now.AddDays(-30));
                    _cache.Add(cacheKey, items, 360);
                }
            }
            return Json(items);
        }

    }
}