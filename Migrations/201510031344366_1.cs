namespace WebApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _1 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Contents", new[] { "PostIdOnSite" });
            DropIndex("dbo.Feeds", new[] { "LastUpdateTime" });
            DropIndex("dbo.Feeds", new[] { "NextCheckTime" });
            CreateTable(
                "dbo.ApplicationUserContents",
                c => new
                    {
                        ApplicationUser_Id = c.String(nullable: false, maxLength: 128),
                        Content_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ApplicationUser_Id, t.Content_Id })
                .ForeignKey("dbo.AspNetUsers", t => t.ApplicationUser_Id, cascadeDelete: true)
                .ForeignKey("dbo.Contents", t => t.Content_Id, cascadeDelete: true)
                .Index(t => t.ApplicationUser_Id)
                .Index(t => t.Content_Id);
            
            AlterColumn("dbo.Contents", "PostIdOnSite", c => c.String(nullable: false, maxLength: 150));
            AlterColumn("dbo.Feeds", "LastCheckTime", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Feeds", "LastUpdateTime", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Feeds", "NextCheckTime", c => c.DateTime(nullable: false));
            CreateIndex("dbo.Contents", "PostIdOnSite");
            CreateIndex("dbo.Feeds", "LastUpdateTime");
            CreateIndex("dbo.Feeds", "NextCheckTime");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ApplicationUserContents", "Content_Id", "dbo.Contents");
            DropForeignKey("dbo.ApplicationUserContents", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.ApplicationUserContents", new[] { "Content_Id" });
            DropIndex("dbo.ApplicationUserContents", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.Feeds", new[] { "NextCheckTime" });
            DropIndex("dbo.Feeds", new[] { "LastUpdateTime" });
            DropIndex("dbo.Contents", new[] { "PostIdOnSite" });
            AlterColumn("dbo.Feeds", "NextCheckTime", c => c.DateTimeOffset(nullable: false, precision: 7));
            AlterColumn("dbo.Feeds", "LastUpdateTime", c => c.DateTimeOffset(nullable: false, precision: 7));
            AlterColumn("dbo.Feeds", "LastCheckTime", c => c.DateTimeOffset(nullable: false, precision: 7));
            AlterColumn("dbo.Contents", "PostIdOnSite", c => c.String(nullable: false));
            DropTable("dbo.ApplicationUserContents");
            CreateIndex("dbo.Feeds", "NextCheckTime");
            CreateIndex("dbo.Feeds", "LastUpdateTime");
            CreateIndex("dbo.Contents", "PostIdOnSite");
        }
    }
}
