﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Core
{
    public class BookmarkItem
    {
        public BookmarkItem(int contentId,string tagId)
        {
            Content = contentId;
            Tag = tagId;
        }

        public int Content { get; private set; }
        public string Tag { get; private set; }
    }
}